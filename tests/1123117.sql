/*
	Testing InsireId: 1123117 (https://hepdata.net/record/ins1126131)
    
    Possible results:
		- OK
        - KO
        - ? -> need investigation
*/

-- TEST: controllo che il numero di tabelle sia corretto
select max(TableNumber)
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
where InspireId = 1123117;
# max(TableNumber)
# 80

-- RESULT: OK

-- TEST: controllo che ci sia solo un asse Y nella tabella 11
select distinct Header
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
where InspireId = 1123117 and TableNumber = 11;
# Header
# (1/Nev)*D2(N)/DYRAP/DPT IN GEV**-1

-- RESULT: OK

-- TEST: prendo tuti i valori della tabella 39 che sono relativi al valore 0.425 oppure 0.275 dell'asse X
select Value
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join XAxesDim xad on xad.ID = yaf.XAXIS_ID_1
where InspireId = 1123117 and TableNumber = 39
	and Focus in (0.425,0.275);
# Value
# 0.3279
# 0.2114
# 0.02296
# 0.02592
# 0.007989

-- RESULT: OK

-- TEST: prendo tutte le propery associate alla tabella 52
select distinct Name
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join PropertiesDim ped on ped.ID = yaf.PROPERTY_ID_1 or ped.ID = yaf.PROPERTY_ID_2 or ped.ID = yaf.PROPERTY_ID_3
where InspireId = 1123117 and TableNumber = 52;
# Name
# SQRT(S)
# MEAN(NAME=MULT(C=TRACKS,TRUE))
# MULT(C-TRACKS,RECON)

-- RESULT: ? manca ABS(YRAP) < 1 ma chissà dove è

-- TEST: prendo le reactions collegata all'esperimento
select distinct red.Name
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join ReactionsDim red on red.ID = yaf.REACTION_ID_1 or red.ID = yaf.REACTION_ID_2 or red.ID = yaf.REACTION_ID_3
where InspireId = 1123117 and TableNumber = 52;
# Name
# P P --> K- X
# P P --> PBAR X
# P P --> PI- X


/* RESULT: ? la reazione c'è anche se su hepdata.net è: P P --> W+- < E+- NUE > X, 
le tre reazioni sono comunque quelle che si ritrovano nelle label di hepdata.net.
*/