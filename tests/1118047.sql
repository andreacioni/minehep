/*
	Testing InsireId: 1118047 (https://hepdata.net/record/ins1118047)
    
    Possible results:
		- OK
        - KO
        - ? -> need investigation
*/

use minehep;

-- TEST: prendo tutte le intestazioni delle colonne dele Y della tabella 1
select distinct Header
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
where InspireId = 1118047 and TableNumber = 1;
# Header
# NNPDF
# MSTW
# HERAPDF
# CT10
# $\mathcal{A}$

-- RESULT: OK

-- TEST: prendo tutte le intestazioni delle colonne dele Y della tabella 2
select distinct Header
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
where InspireId = 1118047 and TableNumber = 2;

-- RESULT: KO mi aspetto che sia vuoto l'Header della Y. Ma non lo è 

-- TEST: prendo tutte le intestazioni delle colonne delle X della tabella 1
select distinct xad.Header
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join XAxesDim xad on xad.ID = yaf.XAXIS_ID_1
where InspireId = 1118047 and TableNumber = 1;
# Header
# $|\eta|$

-- RESULT: OK

-- TEST: prendo tutte le intestazioni delle colonne delle X della tabella 2
select distinct xad.Header
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join XAxesDim xad on xad.ID = yaf.XAXIS_ID_1
where InspireId = 1118047 and TableNumber = 2;
# Header
# $|\eta|$

-- RESULT: OK

-- TEST: prendo tutte le intestazioni delle colonne delle X della tabella 2
select distinct xad.Header
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join XAxesDim xad on xad.ID = yaf.XAXIS_ID_1
where InspireId = 1118047 and TableNumber = 2;

/* RESULT: ? su hepdata.net sembra che ci siano due colonne ma in realtà è una sola 
(possibile che quella colonna venga aggiunta in automatico con il suffisso __1 
per qualhe ragione ignota). La query di riprova su hepdata é:
	select Header from XAxes xax
	inner join Datasets ds on ds.DATASET_ID = xax._dataset_DATASET_ID
	inner join Papers pap on _paper_PAPER_ID = PAPER_ID
	where ds.LocalId = 2 and pap.InspireId = 1118047;
*/

-- TEST: prendo tutte le intestazioni delle colonne dele Y della tabella 2
select distinct yaf.Header, xad.Header
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join XAxesDim xad on xad.ID = yaf.XAXIS_ID_1
where InspireId = 1118047 and TableNumber = 2;

/* RESULT: ? ,in realtà questi dati sono presenti anche sul vecchio schema hepdata, per riprova si può eseguire la seguente query:
	 select distinct Header from YAxes yax
	 inner join Datasets ds on ds.DATASET_ID = yax._dataset_DATASET_ID
	 inner join Papers pap on _paper_PAPER_ID = PAPER_ID
	 where ds.LocalId = 2 and pap.InspireId = 1118047;
*/

-- TEST: prendo la prima propertiy collegata alle misurazioni della tabella 1 e 2
select distinct Name, HighValue, Unit
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join PropertiesDim ped on ped.ID = yaf.PROPERTY_ID_1
where InspireId = 1118047 and (TableNumber = 1 or TableNumber = 2);
# Name, HighValue, Unit
# SQRT(S), 7000, GeV

-- RESULT: OK

-- TEST: prendo gli observable collegati alle misurazioni della tabella 2
select distinct Name
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join ObservableDim obd on obd.ID = yaf.DATASET_OBSERVABLE_ID_1
where InspireId = 1118047 and TableNumber = 2;
# Name
# ASYM

-- RESULT: OK

-- TEST: controllo che i valori delle  Y della tabella 2 siano 121
select count(*)
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
where InspireId = 1118047 and TableNumber = 2;
# count(*)
# 121

-- RESULT: OK

-- TEST: prendo la seconda property collegata alle misurazioni della tabella 2
select distinct Description
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join PropertiesDim ped on ped.ID = yaf.PROPERTY_ID_2
where InspireId = 1118047 and TableNumber = 2;

/* RESULT: ?, non è detto chela property PT(E)	> 35 GEV sia tra questela query di hepdata.net per riprova è:
	select distinct bps.Name from YAxes yax
	inner join Datasets ds on ds.DATASET_ID = yax._dataset_DATASET_ID
	inner join Papers pap on _paper_PAPER_ID = PAPER_ID
	inner join AxisProperties axp on yax.AXIS_ID = axp._yAxis_AXIS_ID
	inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
	where ds.LocalId = 2 and pap.InspireId = 1118047;
*/

-- TEST: prendo tutte le misurazioni dela tabella 1 che sono comprese tra [0.8, 2.0] $|\eta|$ nella colonna HERAPDF 
select distinct Value
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join XAxesDim xad on xad.ID = yaf.XAXIS_ID_1
where InspireId = 1118047 and TableNumber = 1 
	and xad.Header = '$|\\eta|$' 
    and yaf.Header = "HERAPDF" 
    and xad.LowValue >= 0.8 
    and xad.HighValue <= 2.0;
# Value
# 196
# 181
# 153
# 140
# 132

-- RESULT: OK

-- TEST: prendo tutte le misurazioni dela tabella 1 della colonna  $\mathcal{A}$ che hanno un errore SYS +- 6
select yaf.Value
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join YErrorDim yed on yed.ID = yaf.POINT_ERROR_ID_1
where InspireId = 1118047
	and TableNumber = 1
    and Source = 'SYS'
    and Header= '$\\mathcal{A}$'
    and (MinusError=-6 and PlusError=6);
# Value
# 156
# 136

-- RESULT: OK

-- TEST: prendo tutte le misurazioni dela tabella 1 della colonna  $\mathcal{A}$ che hanno un errore STAT +- 4
select Value
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join YErrorDim yed on yed.ID = yaf.POINT_ERROR_ID_2 or yed.ID = yaf.POINT_ERROR_ID_1
where InspireId = 1118047
	and TableNumber = 1
    and Source = 'STAT'
    and Header= '$\\mathcal{A}$'
    and (MinusError=-4 and PlusError=4);
# Value
# 210

-- RESULT: OK

-- TEST: prendo le reactions collegata all'esperimento
select distinct red.Name
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join ReactionsDim red on red.ID = yaf.REACTION_ID_1 or red.ID = yaf.REACTION_ID_2
where InspireId = 1118047;
# Name
# P P --> W- X
# P P --> W+ X


/* RESULT: ? la reazione c'è anche se su hepdata.net è: P P --> W+- < E+- NUE > X, 
le due reazioni sono comunque quelle che si ritrovano nelle label di hepdata.net.
*/