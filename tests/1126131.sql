/*
	Testing InsireId: 1126131 (https://hepdata.net/record/ins1126131)
    
    Possible results:
		- OK
        - KO
        - ? -> need investigation
*/

-- TEST: prendo tutte le intestazioni delle colonne dele Y della tabella 2
select distinct Header
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
where InspireId = 1126131 and TableNumber = 6;

-- RESULT: KO mi aspetto che sia vuoto l'Header della Y. Ma non lo è. Sembrano dei range messi a mano...