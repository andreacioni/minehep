/*
	Testing InsireId: 1392556 (https://hepdata.net/record/ins1392556)
    
    Possible results:
		- OK
        - KO
        - ? -> need investigation
*/

-- TEST: prendo le reactions collegata all'esperimento, tabella 4
select distinct red.Name
from YAxisFacts yaf
inner join PaperDim ppd on ppd.ID = yaf.PAPER_ID
inner join ReactionsDim red on red.ID = yaf.REACTION_ID_1 or red.ID = yaf.REACTION_ID_2 or red.ID = yaf.REACTION_ID_3 or red.ID = yaf.REACTION_ID_4 or red.ID = yaf.REACTION_ID_5 or red.ID = yaf.REACTION_ID_6
where InspireId = 1392556 and TableNumber = 4;
# Name
# PBAR AL --> INELASTIC
# PBAR CD --> INELASTIC
# PBAR C --> INELASTIC
# PBAR TI --> INELASTIC
# PBAR NUCLEUS --> INELASTIC



/* RESULT: ? la reazione c'è anche se su hepdata.net è: P P --> W+- < E+- NUE > X, 
le 5 reazioni sono comunque quelle che si ritrovano nelle label di hepdata.net.
*/