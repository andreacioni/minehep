# minehep
---

Hepdata dump download: https://www.ippp.dur.ac.uk/%7Ewatt/Lenzi/hepdatapublic.dmp.gz_01Apr18

SOSPETTI
-------------------------

- Datasets: 
  - duplicati sia il paper_id che per PK 
    - PRIMARY KEY (`DATASET_ID`),
    - KEY `FK6E8B3F1BF4974CDB` (`_paper_PAPER_ID`),
    - KEY `_paper_PAPER_ID_index` (`_paper_PAPER_ID`),
    - KEY `DATASET_ID_index` (`DATASET_ID`)
  - DataType sempre nullo
- Papers: duplicata PK 
  - PRIMARY KEY (`PAPER_ID`),
  - KEY `PAPER_ID_index` (`PAPER_ID`)
- PaperAuthor: Tanti cognomi anche duplicati; manca la chiave primaria
- DsKeyword contiene quelle keyword che compaiono su HepData come Phrases (**possibile dimesione**)
- BaseProperties sembrerebbe avere più senso collegato direttamente a Datasets

TABELLE VUOTE
-------------------------

  - BaseReactions
  - DatasetProperties: possibile legame con BaseProperties
  - Keywords: il nome dell'indice PK è PAPER_ID ma non c'è nessun collegamento con la tabella Papers

COSE BRUTTE
-------------------------

  - Date in formato VARCHAR
  - Siccome l'engine type è MyISAM, e non InnoDB, non sono ammesse le FOREIGN_KEY
  - **Informazioni sulle reazioni all'interno delle YAxisComment**
  - **NewOldId table probably not needed**

CAMPI TABELLE
-------------------------

  - LocalId è il numero incrementale che indica il numero del Datasets all'interno della Paper (sarebbe la il numero di una tabella di un esperimento specifico) 
  - Error.Comment non è numerico e lo tolgo anche se in realtà può contenere delle informazioni utili.
    - NO NON VA TOLTO perchè sennò sono tutti numeri e non hanno senso se non ci associo quel campo

CAMPI INUTILI
-------------------------
- XAxes.Unit vuoto (ma in realtà dovrebbe essere popolato in futuro)
- DsKeywords.Level sempre a 1

INSPIRE RECORD UTILI
-------------------------

  - ins118808: importante perche' si vede bene dove mettere gli axis_error in relazione con i points_error; se il valore dell'errore è zero viene messo nell'intestazione in alto altrimenti viene trattato come un'errore 

DA DISCUTERE
---
  - _inner join_ anche su campi non chiave in link_data.sql da giustificare (<=>)
  - reactionsDim e observableDim unique
  - non scalabilità delle _multivalued dimensions_
  - potrebbe essere una dimensione anche un fact
  - MyISAM per workaround

ToDo
---
  - Spostare le newOldId in link_table
  - Le date da mettere nel formato giusto se possibile
  - In hepdata la tabella PaperExpts da collegare
  - DsKeyword da collegare (la dimensione c'è già) 
  - Rinominare create_bridges
