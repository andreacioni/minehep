import requests
import argparse
import logging
import copy
import RestResponse

import constants as const
import db_import as db

from schema_entry import StarSchemaEntry

def process_datasets(schema_entry: StarSchemaEntry, paper_data_tables: list):
    dataset_num = 0

    for dataset_meta in paper_data_tables:
        dataset_num += 1
        process_single_dataset(schema_entry, dataset_num, dataset_meta)

def process_single_dataset(schema_entry: StarSchemaEntry, dataset_num: int, dataset_meta: dict):
    assert_present(dataset_meta, 'data')
    assert_present(dataset_meta['data'], 'json')

    json_dataset_url = dataset_meta['data']['json']

    resp = requests.get(json_dataset_url)

    assert resp.status_code in [200,299], 'Can\'t access {}. HTTP Status Code {}:'.format(resp.url, resp.status_code)

    dataset_meta_json = RestResponse.parse(resp.json())
    
    logging.debug('Dataset metadata: %s', dataset_meta_json)

    logging.info('''
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Table %i info:
    - Name: %s
    - Description: %s
<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
''', 
    dataset_num,
    dataset_meta_json.name,
    dataset_meta_json.description
    )

    assert_present(dataset_meta_json, 'values')
    assert_present(dataset_meta_json, 'headers')

    values_array = dataset_meta_json['values']
    
    x_count = len(values_array[0].x)
    y_count = len(values_array[0].y)
    headers_num = len(dataset_meta_json.headers)

    assert (y_count + x_count) == headers_num, 'axis count and headers does not match'

    process_y_values(schema_entry, dataset_num, dataset_meta_json.headers[:x_count], dataset_meta_json.headers[x_count:], values_array)

def process_y_values(schema_entry: StarSchemaEntry, dataset_num, x_headers_array, y_headers_array, values_array):
    for y_idx, y in enumerate(values_array[0].y):
        backup_schema_entry = copy.deepcopy(schema_entry)
        
        for x_idx, x in enumerate(values_array[0].x):
            schema_entry.add_x_axes_entry(header=x_headers_array[x_idx].name, highValue=x.high, lowValue=x.low)

        schema_entry.set_y_fact(header=y_headers_array[y_idx].name, value=y.value, tableNumber=dataset_num)

        db.insert_schema_entry(schema_entry)

        schema_entry = backup_schema_entry 
   

def assert_present(obj: dict, element: str):
    assert element in obj and obj[element] is not None, '"{}" not found in object'.format(element)

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser('hepdata_import.py')
parser.add_argument('inspire_id', type=int, help='the InspireID number associated to the pubblication to add to MineHep data warehouse')

args = parser.parse_args()

assert args.inspire_id >= 0, 'Invalid InspireID supplied!'

resp = requests.get('{}{}?format=json'.format(const.HEPDATA_BASEPATH, args.inspire_id))

assert resp.status_code in [200,299], 'Can\'t access {}. HTTP Status Code {}:'.format(resp.url, resp.status_code)

paper_meta = RestResponse.parse(resp.json())

logging.debug('Paper metadata: %s', paper_meta)

assert_present(paper_meta, 'record')
assert_present(paper_meta, 'data_tables')

paper_record = paper_meta.record
paper_data_tables = paper_meta.data_tables

assert len(paper_data_tables) >= 1, 'No tables associated to InspireID {}'.format(args.inspire_id)

logging.info('''
##############################################################################
Paper info:
 - Title: %s
 - Author: %s
 - DOI: %s
 - Creation date: %s
 - Publication date: %s
 - Last update: %s
 - Table count: %i
##############################################################################
''', 
paper_record.title,
paper_record.author,
paper_record.doi,
paper_record.creation_date,
paper_record.publication_date,
paper_record.last_updated,
len(paper_data_tables))

schema_entry = StarSchemaEntry()

schema_entry.set_paper_entry(
    inspireId=paper_record.inspire_id, 
    title=paper_record.title,
    doi=paper_record.doi
)

for keyword in paper_record.data_keywords.phrases:
   schema_entry.add_keyword_entry(name=keyword) 

process_datasets(schema_entry, paper_data_tables)