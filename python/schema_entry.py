import uuid

class StarSchemaEntry(object):

    def __init__(self):
        self.y_fact = dict()

        self.paper_entry = dict()
        self.keyword_entries = list()
        self.x_axes_entries = list()

    def set_y_fact(self, header=None, tableNumber=None, value=None):
        self.y_fact = {
            'id': str(uuid.uuid1()),
            'header': header,
            'tableNumber': tableNumber,
            'value': value
         }  

    def add_x_axes_entry(self, header=None, highValue=None, lowValue=None):
        self.x_axes_entries.append({
            'id': str(uuid.uuid1()),
            'header': header,
            'highValue': highValue,
            'lowValue': lowValue
        })

    def set_paper_entry(self, inspireId=None, title=None, doi=None, exptName=None, expInformalName=None, expLab=None, pubblicationYear=None):
         self.paper_entry = {
            'id': str(uuid.uuid1()),
            'inspireId': inspireId,
            'title': title,
            'doi': doi,
            'exptName': exptName,
            'expInformalName': expInformalName,
            'expLab': expLab,
            'pubblicationYear': pubblicationYear
         }

    def add_keyword_entry(self, name=None):
        self.keyword_entries.append({
            'id': str(uuid.uuid1()),
            'name': name
         })