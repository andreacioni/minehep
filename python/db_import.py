import logging

import constants as const

from schema_entry import StarSchemaEntry
from peewee import *

mysql_db = MySQLDatabase(const.DATABASE_SCHEMA, user=const.DATABASE_USER, password=const.DATABASE_PASSWORD, host=const.DATABASE_HOST, port=const.DATABASE_PORT)

class BaseModel(Model):
    class Meta:
        database = mysql_db

class PaperDim(BaseModel):
    id = CharField(primary_key=True)
    inspireId = IntegerField()
    title = CharField()
    doi = CharField()
    exptName = CharField()
    expInformalName = CharField()
    expLab = CharField()
    pubblicationYear = IntegerField()

    class Meta:
        db_table = 'PaperDim'

class KeywordsDim(BaseModel):
    id = CharField(primary_key=True)
    name = CharField()

    class Meta:
        db_table = 'KeywordsDim'

class XAxesDim(BaseModel):
    id = CharField(primary_key=True)
    header = CharField()
    highValue = DoubleField()
    lowValue = DoubleField()
    
    class Meta:
        db_table = 'XAxesDim'

class YAxisFacts(BaseModel):
    id = CharField(primary_key=True)
    header = CharField()
    value = DoubleField()
    tableNumber = IntegerField()
    PAPER_ID = ForeignKeyField(PaperDim, db_column='PAPER_ID')
    XAXIS_ID_1 = ForeignKeyField(XAxesDim, db_column='XAXIS_ID_1')
    XAXIS_ID_2 = ForeignKeyField(XAxesDim, db_column='XAXIS_ID_2')
    XAXIS_ID_3 = ForeignKeyField(XAxesDim, db_column='XAXIS_ID_3')
    XAXIS_ID_4 = ForeignKeyField(XAxesDim, db_column='XAXIS_ID_4')
    XAXIS_ID_5 = ForeignKeyField(XAxesDim, db_column='XAXIS_ID_5')
    XAXIS_ID_6 = ForeignKeyField(XAxesDim, db_column='XAXIS_ID_6')
    XAXIS_ID_7 = ForeignKeyField(XAxesDim, db_column='XAXIS_ID_7')
    KEYWORD_ID_1 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_1')
    KEYWORD_ID_2 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_2')
    KEYWORD_ID_3 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_3')
    KEYWORD_ID_4 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_4')
    KEYWORD_ID_5 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_5')
    KEYWORD_ID_6 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_6')
    KEYWORD_ID_7 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_7')
    KEYWORD_ID_8 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_8')
    KEYWORD_ID_9 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_9')
    KEYWORD_ID_10 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_10')
    KEYWORD_ID_11 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_11')
    KEYWORD_ID_12 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_12')
    KEYWORD_ID_13 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_13')
    KEYWORD_ID_14 = ForeignKeyField(KeywordsDim, db_column='KEYWORD_ID_14')

    class Meta:
        db_table = 'YAxisFacts'

def list_get(lst:list, index: int, default):
    if index < len(lst):
        return lst[index]
    return default

def insert_schema_entry(schema_entry: StarSchemaEntry):

    paper_ref = None
    x_refs = list()
    keywords_refs = list()

    #PAPER
    existing_paper = PaperDim.get_or_none(PaperDim.inspireId == schema_entry.paper_entry['inspireId'])

    if existing_paper is None:
        paper_ref = PaperDim.create(
            id = schema_entry.paper_entry['id'],
            inspireId = schema_entry.paper_entry['inspireId'],
            title = schema_entry.paper_entry['title'],
            doi = schema_entry.paper_entry['doi'],
            exptName = schema_entry.paper_entry['exptName'],
            expInformalName = schema_entry.paper_entry['expInformalName'],
            expLab = schema_entry.paper_entry['expLab'],
            pubblicationYear = schema_entry.paper_entry['pubblicationYear']
        )
    else:
        paper_ref = existing_paper

    #KEYWORD
    for keyword in schema_entry.keyword_entries:
        existing_keyword = KeywordsDim.get_or_none(KeywordsDim.name == keyword['name'])

        if existing_keyword is None:
            keywords_refs.append(KeywordsDim.create(
                id = keyword['id'],
                name = keyword['name']
            ))
        else:
            keywords_refs.append(existing_keyword)
    
    #XAXES
    for x in schema_entry.x_axes_entries:
        existing_x = XAxesDim.get_or_none(
            XAxesDim.header == x['header'] and
            XAxesDim.highValue == x['highValue'] and
            XAxesDim.lowValue == x['lowValue']
            )

        if existing_x is None:
            x_refs.append(XAxesDim.create(
                id = x['id'],
                header = x['header'],
                highValue = x['highValue'],
                lowValue = x['lowValue']
            ))
        else:
            x_refs.append(existing_x)
    
    YAxisFacts.create(
        id = schema_entry.y_fact['id'],
        header = schema_entry.y_fact['header'],
        value = schema_entry.y_fact['value'],
        tableNumber = schema_entry.y_fact['tableNumber'],
        PAPER_ID = paper_ref,
        XAXIS_ID_1 = list_get(x_refs, 0, None),
        XAXIS_ID_2 = list_get(x_refs, 1, None),
        XAXIS_ID_3 = list_get(x_refs, 2, None),
        XAXIS_ID_4 = list_get(x_refs, 3, None),
        XAXIS_ID_5 = list_get(x_refs, 4, None),
        XAXIS_ID_6 = list_get(x_refs, 5, None),
        XAXIS_ID_7 = list_get(x_refs, 6, None),
        KEYWORD_ID_1 = list_get(keywords_refs, 0, None),
        KEYWORD_ID_2 = list_get(keywords_refs, 1, None),
        KEYWORD_ID_3 = list_get(keywords_refs, 2, None),
        KEYWORD_ID_4 = list_get(keywords_refs, 3, None),
        KEYWORD_ID_5 = list_get(keywords_refs, 4, None),
        KEYWORD_ID_6 = list_get(keywords_refs, 5, None),
        KEYWORD_ID_7 = list_get(keywords_refs, 6, None),
        KEYWORD_ID_8 = list_get(keywords_refs, 7, None),
        KEYWORD_ID_9 = list_get(keywords_refs, 8, None),
        KEYWORD_ID_10 = list_get(keywords_refs, 9, None),
        KEYWORD_ID_11 = list_get(keywords_refs, 10, None),
        KEYWORD_ID_12 = list_get(keywords_refs, 11, None),
        KEYWORD_ID_13 = list_get(keywords_refs, 12, None),
        KEYWORD_ID_14 = list_get(keywords_refs, 13, None)
    )