SELECT count(*) FROM hepdata.DsKeywords; -- 251403

-- Cerchiamo di capire quante colonne riservare nella tabella dei fatti

select AXIS_ID, count(*) c 
from (
	select distinct AXIS_ID, Keyword
	from DsKeywords dsk
	inner join YAxes yax on yax._dataset_DATASET_ID = dsk.DATASET_ID
) t
group by AXIS_ID
order by c desc
limit 1000; -- 14

select distinct Keyword 
from DsKeywords dsk
inner join YAxes yax on yax._dataset_DATASET_ID = dsk.DATASET_ID
where AXIS_ID = 43450414;

