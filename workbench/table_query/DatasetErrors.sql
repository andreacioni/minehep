select count(*) from DatasetErrors where Comment = "MOMENTUM CALIBRATION";

-- cerchiamo di capire quante colonne riservare nella tabella dei fatti
select pts.VALUE_ID, count(*) c
from Points pts
inner join YAxes yax on pts._yAxis_AXIS_ID = yax.AXIS_ID
inner join DatasetErrors dse on dse.DATASET_ID = _dataset_DATASET_ID
group by pts.VALUE_ID
order by c desc limit 10; -- 19 colonne sono sufficienti

select dse.*
from Points pts
inner join YAxes yax on pts._yAxis_AXIS_ID = yax.AXIS_ID
inner join DatasetErrors dse on dse.DATASET_ID = _dataset_DATASET_ID
where VALUE_ID = 92209260;