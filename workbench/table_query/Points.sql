SELECT _yAxis_AXIS_ID FROM hepdata.Points;

select distinct ConfLevel from Points;
 
select distinct Relation from Points;

select * from Points pts
inner join PointErrors pte on pte.VALUE_ID = pts.VALUE_ID;

select VALUE_ID, LocalId
from Points
order by VALUE_ID;

select distinct Relation
from Points;

-- Quante relazioni per i vari punti sono disponibili
select Relation, count(*)
from Points
group by Relation;