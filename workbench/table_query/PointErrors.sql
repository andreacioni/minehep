SELECT * FROM hepdata.PointErrors;

select  distinct PlusErrorLength from PointErrors;

select distinct Description from PointErrors;

select  distinct Source from PointErrors;

select VALUE_ID, count(*) c
from PointErrors
group by VALUE_ID
order by c desc; /*molti valori di y (VALUE_ID) hanno 71 errori collegati, un esempio: 312545663, 288293713, 269420866, 308937447, 274432424,274432513*/

select VALUE_ID, LocalId, count(*) c
from PointErrors
group by VALUE_ID, LocalId having c > 1;

select c, count(c) cc from (
	select VALUE_ID, count(*) c
	from PointErrors
	group by VALUE_ID) a
group by c
order by cc;  /*numero di errori collegati a un VALUE_ID*/

-- cerchiamo di capire chi sono i dataset e le paper che hanno questo comportamento strano
select ds.DATASET_ID , pap.*
from Points pts
inner join YAxes yax on pts._yAxis_AXIS_ID = yax.AXIS_ID
inner join Datasets ds on ds.DATaSET_ID = yax._dataset_DATASET_ID
inner join Papers pap on pap.PAPER_ID = ds._paper_PAPER_ID
where pts.VALUE_ID = 312545663;

select *
from PointErrors pte
inner join Points pts on pts.VALUE_ID = pte.VALUE_ID
where pts._yAxis_AXIS_ID = 163840002;

-- Source differenti
select distinct Source
from PointErrors pte;

-- cerchiamo di capire quante colonne riservare nella tabella dei fatti
select pte.VALUE_ID, count(*) c
from PointErrors pte
group by pte.VALUE_ID
order by c desc limit 1; -- 71 colonne sono sufficienti

-- troviamo l'asse corrispondente
select _yAxis_AXIS_ID, pte.*
from PointErrors pte
inner join Points pts on pte.VALUE_ID = pts.VALUE_ID
where pte.VALUE_ID = 193429506;
