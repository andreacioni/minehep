SELECT * FROM hepdata.DsReactions; -- 121171

-- Cerchiamo di capire quante colonne riservare nella tabella dei fatti

select AXIS_ID, count(*) c 
from DsReactions dsr
inner join YAxes yax on yax._dataset_DATASET_ID = dsr.DATASET_ID
group by AXIS_ID
order by c desc
limit 100; -- 160

-- In questa tabella sembrano esserci quelli che su 'hepdata' sono considerati reactions. 
-- Adesso bisogna capire cosa c'è dentro le AxisReactions

select _yAxis_AXIS_ID, count(*) c 
from AxisReactions dsr
group by _yAxis_AXIS_ID
order by c desc; -- 34

--
select c , count(*) d
from (
	select _yAxis_AXIS_ID, count(*) c 
	from AxisReactions dsr
	group by _yAxis_AXIS_ID
) a
group by c
order by c desc; -- 

-- dividiamo in Initial e Final
select _yAxis_AXIS_ID, count(*) c 
from AxisReactions dsr
inner join InitialStates ini on ini.REACTION_ID = dsr.REACTION_ID
group by _yAxis_AXIS_ID
order by c desc
limit 100; -- 68

select _yAxis_AXIS_ID, count(*) c 
from AxisReactions dsr
inner join FinalStates fin on fin.REACTION_ID = dsr.REACTION_ID
group by _yAxis_AXIS_ID
order by c desc
limit 100; -- 73

-- raggruppiamo anche per REACTION_ID per non perdere la relazione tra Initial and Final state
select _yAxis_AXIS_ID, dsr.REACTION_ID, count(*) c 
from AxisReactions dsr
inner join InitialStates ini on ini.REACTION_ID = dsr.REACTION_ID
group by _yAxis_AXIS_ID, dsr.REACTION_ID
order by c desc
limit 100; -- 3

select _yAxis_AXIS_ID, dsr.REACTION_ID, count(*) c 
from AxisReactions dsr
inner join FinalStates fin on fin.REACTION_ID = dsr.REACTION_ID
group by _yAxis_AXIS_ID, dsr.REACTION_ID
order by c desc
limit 100; -- 16

-- 34*(16 + 3) = 646 è un numero un po' impegnativo forse non conviene

-- contiamo quanto sarebbero grandi le dimensioni

select count(*)
from (
	select distinct fin.MultRelation, fin.Multiplicity, fin.ParticleName
    from FinalStates fin
) tmp
union
select count(*)
from (
	select distinct ini.MultRelation, ini.Multiplicity, ini.ParticleName
    from InitialStates ini
) tmp
union
select count(*)
from (
	select distinct ini.MultRelation, ini.Multiplicity, ini.ParticleName
    from InitialStates ini
    union
    select distinct fin.MultRelation, fin.Multiplicity, fin.ParticleName
    from FinalStates fin
) tmp
union
select 2233+522;

-- si deve controllare che per ogni reazione ci sia almeno un initial e un final

select count(*) from AxisReactions; -- 163608

select count(*)
from (
	select distinct REACTION_ID from FinalStates
) t;

select count(*)
from (
	select distinct REACTION_ID from InitialStates
) t;




-- cerchiamo informazioni aggiuntive anche nella tabelle delle DsReactions
select distinct DsReactions, Posn
from DsReactions dsr
inner join YAxes yax on yax._dataset_DATASET_ID = dsr.DATASET_ID
where AXIS_ID = 14680077;

-- cerchiamo informazioni nelle tabelle Initial/Final
select distinct ini.*, fin.*
from AxisReactions dsr
inner join InitialStates ini on dsr.REACTION_ID = ini.REACTION_ID
inner join FinalStates fin on dsr.REACTION_ID = fin.REACTION_ID
where _yAxis_AXIS_ID = 14680077;

-- cerchiamo informazioni nelle tabelle Final
select distinct fin.*
from AxisReactions dsr
inner join FinalStates fin on dsr.REACTION_ID = fin.REACTION_ID
where _yAxis_AXIS_ID = 225607688;