-- cosa è Relation?

select distinct Relation
from Bins;

-- è la relazione delle misure, vediamo quante x sono associate ad un y, al massimo

select pts.VALUE_ID, count(*) c
from XAxes xax
inner join Bins bns on bns._xAxis_AXIS_ID = xax.AXIS_ID
inner join YAxes yax on yax._dataset_DATASET_ID = xax._dataset_DATASET_ID
inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
group by pts.VALUE_ID
order by c desc limit 100; -- NON FUNZIONA!

-- adesso cerchiamo di capire di quante colonne nella tabella dei fatti sono richieste al massimo

select val_id, count(*) c
from (
	select distinct pts.VALUE_ID val_id, xax.Header, bns.Focus, bns.FocusLength, bns.HighValue, bns.HighValueLength, bns.LowValue, bns.LowValueLength, bns.Relation, bns.Width
	from XAxes xax
	inner join Bins bns on bns._xAxis_AXIS_ID = xax.AXIS_ID
    inner join YAxes yax on yax._dataset_DATASET_ID = xax._dataset_DATASET_ID
    inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
) temp
group by val_id
order by c desc limit 5; 

select Y, count(*) c
from (
	select distinct xax.AXIS_ID X, yax.AXIS_ID Y
	from XAxes xax
	inner join YAxes yax on yax._dataset_DATASET_ID = xax._dataset_DATASET_ID
) tmp
group by Y
order by c desc limit 100; -- 7

-- testiamo il risultato (importante il join utilizzando il LocalId)
select distinct pts.VALUE_ID, xad.Header, xad.Focus, pts.Value
from XAxes xax
inner join Bins bns on bns._xAxis_AXIS_ID = xax.AXIS_ID 
inner join YAxes yax on yax._dataset_DATASET_ID = xax._dataset_DATASET_ID 
inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID and bns.LocalId = pts.LocalId
inner join minehep.XAxesDim xad on xad.Header <=> xax.Header and xad.Focus <=> bns.Focus and xad.FocusLength <=> bns.FocusLength and xad.HighValue <=> bns.HighValue and xad.HighValueLength <=> bns.HighValueLength and xad.LowValue <=> bns.LowValue and xad.LowValueLength <=> bns.LowValueLength and xad.Width <=> bns.Width
where yax.AXIS_ID = 10813628;

-- FINE

select Header from XAxes xax
inner join Datasets ds on ds.DATASET_ID = xax._dataset_DATASET_ID
inner join Papers pap on _paper_PAPER_ID = PAPER_ID
where ds.LocalId = 2 and pap.InspireId = 1118047;