SELECT * FROM hepdata.Datasets; -- 71583

select * from hepdata.YAxes
where _dataset_DATASET_ID="86732";

-- selezionare inspireID da dataset

select InspireId, ds.LocalId
from Papers p 
inner join Datasets ds on _paper_PAPER_ID = PAPER_ID
where DATASET_ID = 36993; 


-- selezionare inspireId da YAXIS_ID
select InspireId, ds.LocalId, ds.DATASET_ID, p.PAPER_ID
from Papers p
inner join Datasets ds on _paper_PAPER_ID = PAPER_ID
inner join YAxes yax on yax._dataset_DATASET_ID = ds.DATASET_ID
where yax.AXIS_ID = 180060166;

-- selezionare inspireId da pts.VALUE_ID;
select InspireId, ds.LocalId, ds.DATASET_ID, p.PAPER_ID
from Papers p
inner join Datasets ds on _paper_PAPER_ID = PAPER_ID
inner join YAxes yax on yax._dataset_DATASET_ID = ds.DATASET_ID
inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
where pts.VALUE_ID = 10782297;