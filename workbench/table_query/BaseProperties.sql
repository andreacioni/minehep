SELECT * FROM hepdata.BaseProperties; -- 24000 

-- Cerchiamo di uniformare le unità di misura

select Unit, count(*)
from BaseProperties
group by Unit;

-- 

select distinct Name
from BaseProperties;

select distinct Focus, HighValue, LowValue, Name, Unit
from BaseProperties;

-- Cerchiamo di capire quanta roba diversa c'è

select Name, count(*)
from BaseProperties
group by Name;

select distinct Name from BaseProperties; -- 890 

SELECT * FROM hepdata.BaseProperties where Name='SQRT(S)'; -- 103913

SELECT * FROM hepdata.BaseProperties where Name='SQRT(S)' and Unit='GeV'; -- 103461 (sono quasi tutti Gev le unità di misura)

-- Vediamo se riusciamo ad associare un'energia ad ogni dataset

select distinct _dataset_DATASET_ID, HighValue, LowValue, Focus
from YAxes yax
inner join AxisProperties axp on yax.AXIS_ID = axp._yAxis_AXIS_ID
inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
where Name='SQRT(S)'; -- 47388

select distinct _dataset_DATASET_ID, HighValue, LowValue, Focus
from YAxes yax
inner join AxisProperties axp on yax.AXIS_ID = axp._yAxis_AXIS_ID
inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
where Name='SQRT(S)' and Unit='GeV'; -- 47276

select distinct _dataset_DATASET_ID, HighValue, LowValue, Focus
from YAxes yax
inner join AxisProperties axp on yax.AXIS_ID = axp._yAxis_AXIS_ID
inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
where Name='SQRT(S)' and Unit='GeV' and Focus is not null; -- 0

-- considerando che i dataset sono circa 71000 dobbiamo raffinare la ricerca

select _dataset_DATASET_ID, HighValue as SQRT,count(*)
from YAxes yax
inner join AxisProperties axp on yax.AXIS_ID = axp._yAxis_AXIS_ID
inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
where Name='SQRT(S)' and Unit='GeV'
group by _dataset_DATASET_ID, SQRT; -- 35903

-- raggruppiamo le proprietà dei punti degli assi in modo da averne solo 1 per ogni AXIS_ID

select _yAxis_AXIS_ID, HighValue, LowValue, count(*) -- il Focus è escluso perchè è sempre nullo
from AxisProperties axp
inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
where Name='SQRT(S)' and Unit='GeV'
group by _yAxis_AXIS_ID, HighValue, LowValue; -- 96029 

-- non ha senso concentrarsi solo su una properties

select _dataset_DATASET_ID, Focus, HighValue, LowValue, Name, Unit, count(*)
from YAxes yax
inner join AxisProperties axp on yax.AXIS_ID = axp._yAxis_AXIS_ID
inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
group by _dataset_DATASET_ID, Focus, HighValue, LowValue, Name, Unit;

-- cerchiamo quante colonne devo riservare alle base properties, al massimo, nella tabella Facts
select AXIS_ID ax_id, count(*) c
from YAxes yax
inner join AxisProperties axp on yax.AXIS_ID = axp._yAxis_AXIS_ID
inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
group by ax_id
order by c desc;  -- 35 colonne sono necessarie al massimo per ogni base property

select Focus, HighValue, LowValue, Name, Unit
from YAxes yax
inner join AxisProperties axp on yax.AXIS_ID = axp._yAxis_AXIS_ID
inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
where AXIS_ID = 276070600;

-- per la tabella delle dimensioni si usa la seguente query:
select distinct Focus, HighValue, LowValue, Name, Unit
from BaseProperties; -- 21354

-- assegnamo un id univoco incrementale a tutti i vari valori
set @row_number := floor(rand()*10*rand()*10);
drop table if exists temp;
create temporary table temp
	select @row_number := 1 + @row_number as ID, t.*
	from (
		select distinct Focus, HighValue, LowValue, Name, Unit
		from BaseProperties
	) t;


select * from temp;

-- associo vecchie chiavi con nuove chiavi
select ID, PROPERTY_ID
from temp b, BaseProperties bps 
where bps.Focus <=> b.Focus and bps.HighValue <=> b.HighValue and bps.LowValue <=> b.LowValue and bps.Name <=> b.Name and bps.Unit <=> b.Unit
order by ID;

-- FINE

select distinct bps.Name, yax.AXIS_ID from YAxes yax
inner join Datasets ds on ds.DATASET_ID = yax._dataset_DATASET_ID
inner join Papers pap on _paper_PAPER_ID = PAPER_ID
inner join AxisProperties axp on yax.AXIS_ID = axp._yAxis_AXIS_ID
inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
where ds.LocalId = 12 and pap.InspireId = 1123117;