SELECT count(*) FROM hepdata.YAxes where Observable is null or Observable = '' or Observable = ' ';

SELECT * FROM hepdata.YAxes where Observable is null or Observable = '' or Observable = ' ';

-- cerchiamo chi non ha un observable valido (43985)
select *
from YAxes yax
where Observable IS NULL OR Observable = '' or Observable = ' ';

-- di alcuni di questi valori non me ne faccio nulla perchè non ho idea dell'observable a cui
-- sono collegati. Il numero è abbastanza esiguo 2397 e quindi ricercando i dataset a cui sono 
-- collegati si può pensare di eliminarli
SELECT 
    yax._dataset_DATASET_ID,
    yax.AXIS_ID axid,
    yax.Observable,
    COUNT(*) cc,
    GROUP_CONCAT(DISTINCT dso.DsObservables separator '__SEPARATOR__') obs
FROM
    DsObservables dso
        INNER JOIN
    Datasets ds ON dso.DATASET_ID = ds.DATASET_ID
        INNER JOIN
    YAxes yax ON ds.DATASET_ID = yax._dataset_DATASET_ID
WHERE
    Observable IS NULL OR Observable = '' or Observable = ' '
GROUP BY yax._dataset_DATASET_ID , yax.AXIS_ID , yax.Observable
HAVING cc <> 1 and obs not like '%__SEPARATOR__%';

-- qui di seguito ricerco i dataset che contengono questi valori;
-- sono 70 i dataset che andrebbero rimossi perchè privi di un collegamento tra la misurazione e
-- il rispettivo observable
SELECT DISTINCT
    ddid
FROM
    (SELECT 
		yax._dataset_DATASET_ID ddid,
		yax.AXIS_ID axid,
		yax.Observable,
		COUNT(*) cc,
		GROUP_CONCAT(DISTINCT dso.DsObservables separator '__SEPARATOR__') obs
	FROM
		DsObservables dso
			INNER JOIN
		Datasets ds ON dso.DATASET_ID = ds.DATASET_ID
			INNER JOIN
		YAxes yax ON ds.DATASET_ID = yax._dataset_DATASET_ID
	WHERE
		Observable IS NULL OR Observable = '' or Observable = ' '
	GROUP BY yax._dataset_DATASET_ID , yax.AXIS_ID , yax.Observable
	HAVING cc <> 1 and obs not like '%__SEPARATOR__%') AS t;

-- Però alcuni non sono da buttare via (39802):
select yax.AXIS_ID AXIS_ID, count(*) cnt, group_concat(distinct dso.DsObservables) newObsevable
from DsObservables dso
inner join Datasets ds on dso.DATASET_ID = ds.DATASET_ID
inner join YAxes yax on ds.DATASET_ID = yax._dataset_DATASET_ID
where Observable is null or Observable = '' or Observable = ' '
group by yax.AXIS_ID having cnt = 1 or newObsevable not like '%__SEPARATOR__%';

-- per quanto riguarda gli yax.Observable nulli o vuoti si può procedere a spostare i valori della
-- tabella DsObservable (che contengono un unico valore per un dato dataset) all'interno di questo campo
-- con il seguente script:
drop table if exists _tempobs;
create temporary table _tempobs
	select yax.AXIS_ID AXIS_ID, count(*) cnt, group_concat(distinct dso.DsObservables) newObsevable
	from DsObservables dso
	inner join Datasets ds on dso.DATASET_ID = ds.DATASET_ID
	inner join YAxes yax on ds.DATASET_ID = yax._dataset_DATASET_ID
	where Observable is null or Observable = '' or Observable = ' '
	group by yax.AXIS_ID having cnt = 1 or newObsevable not like '%__SEPARATOR__%';

SET SQL_SAFE_UPDATES = 0;
UPDATE YAxes yax, _tempobs 
SET 
    yax.Observable = _tempobs.newObsevable
WHERE
    yax.AXIS_ID = _tempobs.AXIS_ID;
SET SQL_SAFE_UPDATES = 1;

-- per quelli multipli che non si possono associare in nessun modo si procede
-- all'eliminazione


-- FINE

select * from YAxes
where Observable like '%__SEPARATOR__%';

select * from YAxes where AXIS_ID = 81100815;

select *
from hepdata.YAxes yax
inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
where pts.VALUE_ID = 274432424;

select *
from hepdata.YAxes yax
inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
where yax.AXIS_ID = 142606357;

select distinct Header from YAxes yax
inner join Datasets ds on ds.DATASET_ID = yax._dataset_DATASET_ID
inner join Papers pap on _paper_PAPER_ID = PAPER_ID
where ds.LocalId = 2 and pap.InspireId = 1118047