-- 74799 righe
select * from DsObservables;

select AXIS_ID, Observable, count(*) c
from YAxes
group by AXIS_ID, Observable
order by c desc
limit 100;

select AXIS_ID, Observable
from YAxes
where Observable is not null or Observable != '' or trim(Observable) != ''; -- 108740

select AXIS_ID, DsObservables
from Datasets ds
inner join DsObservables dso on dso.DATASET_ID = ds.DATASET_ID
inner join YAxes yax on yax._dataset_DATASET_ID = ds.DATASET_ID;

select AXIS_ID, DsObservables, Header
from Datasets ds
inner join DsObservables dso on dso.DATASET_ID = ds.DATASET_ID
inner join YAxes yax on yax._dataset_DATASET_ID = ds.DATASET_ID
where AXIS_ID = 227180579
union
select AXIS_ID, Observable, ''
from YAxes
where AXIS_ID = 227180579;

select distinct AXIS_ID, DsObservables
from DsObservables dso
inner join YAxes yax on yax._dataset_DATASET_ID = dso.DATASET_ID
where AXIS_ID = 87949316
order by AXIS_ID;

select AXIS_ID, count(*) c
from (
	select distinct AXIS_ID, DsObservables
	from DsObservables dso
	inner join YAxes yax on yax._dataset_DATASET_ID = dso.DATASET_ID
) t
group by AXIS_ID
order by c desc
limit 100;

select AXIS_ID, DsObservables
from DsObservables dso
inner join YAxes yax on yax._dataset_DATASET_ID = dso.DATASET_ID
where AXIS_ID = 87949316;

select Observable from YAxes where AXIS_ID = 227180579;

select *
from (
	select _dataset_DATASET_ID, AXIS_ID, Observable
	from YAxes
	where Observable is not null or Observable != '' or trim(Observable) != ''
) t
inner join DsObservables dso on DATASET_ID = _dataset_DATASET_ID;