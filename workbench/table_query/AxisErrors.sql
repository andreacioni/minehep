SELECT * FROM hepdata.AxisErrors;

SELECT * FROM hepdata.AxisErrors
where AXIS_ID = 142606357;

select AXIS_ID, count(*)
from AxisErrors
group by AXIS_ID;

select * from AxisErrors
where AXIS_ID = 119963649;

select ds.DATASET_ID, ds.LocalId, pap.*, axe.*
from AxisErrors axe
inner join YAxes yax on axe.AXIS_ID = yax.AXIS_ID
inner join Datasets ds on ds.DATaSET_ID = yax._dataset_DATASET_ID
inner join Papers pap on pap.PAPER_ID = ds._paper_PAPER_ID
where axe.AXIS_ID = 163840002;

select distinct MinusErrorLength
from AxisErrors;

-- cerchiamo quante colonne devo riservare, al massimo, nella tabella dei fatti
select VALUE_ID, count(*) c
from AxisErrors axe
inner join Points pts on axe.AXIS_ID = pts._yAxis_AXIS_ID
group by VALUE_ID
order by c desc
limit 10; -- 7

-- troviamo l'asse corrispondente
select axe.*
from AxisErrors axe
inner join Points pts on axe.AXIS_ID = pts._yAxis_AXIS_ID
where VALUE_ID = 193429506;