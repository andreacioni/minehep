select count(*) from Papers; -- 8429

select count(*) from PaperExpts; -- 5682

select pap.PAPER_ID, count(*) c
from Papers pap
left join PaperExpts prf on prf.PAPER_ID = pap.PAPER_ID
group by PAPER_ID having c = 1; -- 8429

-- è una relazione 1:1 la metto nelle collonne della dimensione Paper!

select distinct ExptName from PaperExpts;