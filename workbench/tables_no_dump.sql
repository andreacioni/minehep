-- MySQL dump 10.13  Distrib 5.1.63, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: hepdata
-- ------------------------------------------------------
-- Server version	5.1.63-0ubuntu0.10.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AxisErrors`
--

DROP TABLE IF EXISTS `AxisErrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AxisErrors` (
  `AXIS_ID` bigint(20) NOT NULL,
  `LocalId` int(11) NOT NULL,
  `Comment` text,
  `MinusError` double NOT NULL,
  `MinusErrorLength` int(11) DEFAULT NULL,
  `Norm` varchar(255) NOT NULL,
  `PlusError` double NOT NULL,
  `PlusErrorLength` int(11) DEFAULT NULL,
  `Source` varchar(255) NOT NULL,
  PRIMARY KEY (`AXIS_ID`,`LocalId`,`MinusError`,`Norm`,`PlusError`,`Source`),
  KEY `FK61BC228C1D3D1D62` (`AXIS_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `AxisProperties`
--

DROP TABLE IF EXISTS `AxisProperties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AxisProperties` (
  `PROPERTY_ID` bigint(20) NOT NULL,
  `_yAxis_AXIS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`PROPERTY_ID`),
  KEY `FK70F3C7D42F51580B` (`PROPERTY_ID`),
  KEY `FK70F3C7D4B9695FBE` (`_yAxis_AXIS_ID`),
  KEY `PROPERTY_ID_index` (`PROPERTY_ID`),
  KEY `_yAxis_AXIS_ID_index` (`_yAxis_AXIS_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `AxisReactions`
--

DROP TABLE IF EXISTS `AxisReactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AxisReactions` (
  `REACTION_ID` bigint(20) NOT NULL,
  `_yAxis_AXIS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`REACTION_ID`),
  KEY `FK2DD7D8A9B9695FBE` (`_yAxis_AXIS_ID`),
  KEY `_yAxis_AXIS_ID_index` (`_yAxis_AXIS_ID`),
  KEY `REACTION_ID_index` (`REACTION_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `BaseProperties`
--

DROP TABLE IF EXISTS `BaseProperties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BaseProperties` (
  `PROPERTY_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Focus` double DEFAULT NULL,
  `HighValue` double NOT NULL,
  `LowValue` double NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Unit` varchar(255) NOT NULL,
  PRIMARY KEY (`PROPERTY_ID`),
  KEY `PROPERTY_ID_index` (`PROPERTY_ID`),
  KEY `Name_index` (`Name`),
  KEY `HighValue_index` (`HighValue`),
  KEY `LowValue_index` (`LowValue`)
) ENGINE=MyISAM AUTO_INCREMENT=308195 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `BaseReactions`
--

DROP TABLE IF EXISTS `BaseReactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BaseReactions` (
  `REACTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`REACTION_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `Bins`
--

DROP TABLE IF EXISTS `Bins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bins` (
  `VALUE_ID` bigint(20) NOT NULL,
  `Focus` double DEFAULT NULL,
  `FocusLength` int(11) DEFAULT NULL,
  `HighValue` double DEFAULT NULL,
  `HighValueLength` int(11) DEFAULT NULL,
  `LocalId` int(11) NOT NULL,
  `LowValue` double DEFAULT NULL,
  `LowValueLength` int(11) DEFAULT NULL,
  `Relation` varchar(255) NOT NULL,
  `Width` double DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `_xAxis_AXIS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`VALUE_ID`),
  KEY `FK1F986CCAD841BC` (`_xAxis_AXIS_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `DatasetComments`
--

DROP TABLE IF EXISTS `DatasetComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DatasetComments` (
  `DATASET_ID` bigint(20) NOT NULL,
  `Comments` text,
  `Posn` int(11) NOT NULL,
  PRIMARY KEY (`DATASET_ID`,`Posn`),
  KEY `FKC3AAB66CC83ABA29` (`DATASET_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `DatasetErrors`
--

DROP TABLE IF EXISTS `DatasetErrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DatasetErrors` (
  `DATASET_ID` bigint(20) NOT NULL,
  `LocalId` int(11) NOT NULL,
  `Comment` text,
  `MinusError` double NOT NULL,
  `MinusErrorLength` int(11) DEFAULT NULL,
  `Norm` varchar(255) NOT NULL,
  `PlusError` double NOT NULL,
  `PlusErrorLength` int(11) DEFAULT NULL,
  `Source` varchar(255) NOT NULL,
  PRIMARY KEY (`DATASET_ID`,`LocalId`,`MinusError`,`Norm`,`PlusError`,`Source`),
  KEY `FK57B405E3C83ABA29` (`DATASET_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `DatasetProperties`
--

DROP TABLE IF EXISTS `DatasetProperties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DatasetProperties` (
  `PROPERTY_ID` bigint(20) NOT NULL,
  `_dataset_DATASET_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`PROPERTY_ID`),
  KEY `FK170C27AB2F51580B` (`PROPERTY_ID`),
  KEY `FK170C27AB3BAA6EF` (`_dataset_DATASET_ID`),
  KEY `PROPERTY_ID_index` (`PROPERTY_ID`),
  KEY `_dataset_DATASET_ID_index` (`_dataset_DATASET_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `Datasets`
--

DROP TABLE IF EXISTS `Datasets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Datasets` (
  `DATASET_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LocalId` int(11) NOT NULL,
  `_paper_PAPER_ID` bigint(20) DEFAULT NULL,
  `DataType` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`DATASET_ID`),
  KEY `FK6E8B3F1BF4974CDB` (`_paper_PAPER_ID`),
  KEY `_paper_PAPER_ID_index` (`_paper_PAPER_ID`),
  KEY `DATASET_ID_index` (`DATASET_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=91957 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `DsKeywords`
--

DROP TABLE IF EXISTS `DsKeywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DsKeywords` (
  `DATASET_ID` bigint(20) NOT NULL,
  `LocalId` int(11) NOT NULL,
  `Posn` int(11) NOT NULL,
  `Level` int(11) DEFAULT NULL,
  `Keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DATASET_ID`,`LocalId`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `DsObservables`
--

DROP TABLE IF EXISTS `DsObservables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DsObservables` (
  `DATASET_ID` bigint(20) NOT NULL,
  `DsObservables` varchar(255) DEFAULT NULL,
  `Posn` int(11) NOT NULL,
  PRIMARY KEY (`DATASET_ID`,`Posn`),
  KEY `FK3A475F61C83ABA29` (`DATASET_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `DsPlabs`
--

DROP TABLE IF EXISTS `DsPlabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DsPlabs` (
  `DATASET_ID` bigint(20) NOT NULL DEFAULT '0',
  `DsPlabs` text,
  `Posn` int(11) NOT NULL,
  PRIMARY KEY (`DATASET_ID`,`Posn`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `DsReactions`
--

DROP TABLE IF EXISTS `DsReactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DsReactions` (
  `DATASET_ID` bigint(20) NOT NULL,
  `DsReactions` varchar(255) DEFAULT NULL,
  `Posn` int(11) NOT NULL,
  PRIMARY KEY (`DATASET_ID`,`Posn`),
  KEY `FK7E3FD77BC83ABA29` (`DATASET_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `FinalStates`
--

DROP TABLE IF EXISTS `FinalStates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FinalStates` (
  `REACTION_ID` bigint(20) NOT NULL,
  `MultRelation` int(11) DEFAULT NULL,
  `Multiplicity` int(11) NOT NULL,
  `ParticleName` varchar(255) NOT NULL,
  `PDGCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`REACTION_ID`,`Multiplicity`,`ParticleName`),
  KEY `ParticleName_index` (`ParticleName`),
  KEY `REACTION_ID_index` (`REACTION_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `InitialStates`
--

DROP TABLE IF EXISTS `InitialStates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `InitialStates` (
  `REACTION_ID` bigint(20) NOT NULL,
  `MultRelation` int(11) DEFAULT NULL,
  `Multiplicity` int(11) NOT NULL,
  `ParticleName` varchar(255) NOT NULL,
  `PDGCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`REACTION_ID`,`Multiplicity`,`ParticleName`),
  KEY `ParticleName_index` (`ParticleName`),
  KEY `REACTION_ID_index` (`REACTION_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `Keywords`
--

DROP TABLE IF EXISTS `Keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Keywords` (
  `PAPER_ID` bigint(20) NOT NULL,
  `LocalId` int(11) NOT NULL,
  `Posn` int(11) NOT NULL,
  `Level` int(11) DEFAULT NULL,
  `Keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PAPER_ID`,`LocalId`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `PaperAuthors`
--

DROP TABLE IF EXISTS `PaperAuthors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PaperAuthors` (
  `PAPER_ID` bigint(20) NOT NULL,
  `element` varchar(255) DEFAULT NULL,
  KEY `FKB042553C8170C169` (`PAPER_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `PaperComments`
--

DROP TABLE IF EXISTS `PaperComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PaperComments` (
  `PAPER_ID` bigint(20) NOT NULL,
  `element` text,
  `Posn` int(11) NOT NULL,
  PRIMARY KEY (`PAPER_ID`,`Posn`),
  KEY `FKDEB537808170C169` (`PAPER_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `PaperExpts`
--

DROP TABLE IF EXISTS `PaperExpts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PaperExpts` (
  `PAPER_ID` bigint(20) NOT NULL,
  `Collider` varchar(255) DEFAULT NULL,
  `ExptComment` text,
  `InformalName` varchar(255) DEFAULT NULL,
  `Lab` varchar(255) DEFAULT NULL,
  `ExptName` varchar(255) NOT NULL,
  PRIMARY KEY (`PAPER_ID`,`ExptName`),
  KEY `FK9CC777508170C169` (`PAPER_ID`),
  KEY `InformalName_index` (`InformalName`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `PaperMods`
--

DROP TABLE IF EXISTS `PaperMods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PaperMods` (
  `PAPER_ID` bigint(20) NOT NULL,
  `ModComment` text,
  `Modifier` varchar(255) DEFAULT NULL,
  `ModAction` text,
  KEY `FK8933393D8170C169` (`PAPER_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `PaperRefs`
--

DROP TABLE IF EXISTS `PaperRefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PaperRefs` (
  `PAPER_ID` bigint(20) NOT NULL,
  `Comment` varchar(255) NOT NULL,
  `Date` varchar(255) DEFAULT NULL,
  `Description` varchar(255) NOT NULL,
  `Type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PAPER_ID`,`Comment`,`Description`),
  KEY `FK893559CC8170C169` (`PAPER_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `Papers`
--

DROP TABLE IF EXISTS `Papers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Papers` (
  `PAPER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HepdataId` bigint(20) NOT NULL,
  `RedId` bigint(20) DEFAULT NULL,
  `SpiresId` bigint(20) DEFAULT NULL,
  `InspireId` bigint(20) DEFAULT NULL,
  `CdsId` bigint(20) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `DOI` varchar(80) DEFAULT NULL,
  `DateUpdated` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`PAPER_ID`),
  UNIQUE KEY `HepdataId` (`HepdataId`),
  KEY `PAPER_ID_index` (`PAPER_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=9341 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `PointErrors`
--

DROP TABLE IF EXISTS `PointErrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PointErrors` (
  `VALUE_ID` bigint(20) NOT NULL,
  `LocalId` int(11) NOT NULL,
  `Comment` text,
  `MinusError` double NOT NULL,
  `MinusErrorLength` int(11) DEFAULT NULL,
  `Norm` varchar(255) NOT NULL,
  `PlusError` double NOT NULL,
  `PlusErrorLength` int(11) DEFAULT NULL,
  `Source` varchar(255) NOT NULL,
  PRIMARY KEY (`VALUE_ID`,`LocalId`,`MinusError`,`Norm`,`PlusError`,`Source`),
  KEY `FK5AAFECFBEAC63F68` (`VALUE_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `Points`
--

DROP TABLE IF EXISTS `Points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Points` (
  `VALUE_ID` bigint(20) NOT NULL,
  `ConfLevel` double DEFAULT NULL,
  `LocalId` int(11) NOT NULL,
  `Relation` varchar(255) DEFAULT NULL,
  `Value` double NOT NULL,
  `ValueLength` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `_yAxis_AXIS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`VALUE_ID`),
  KEY `FK8ED14903B9695FBE` (`_yAxis_AXIS_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `Systematics`
--

DROP TABLE IF EXISTS `Systematics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Systematics` (
  `PAPER_ID` bigint(20) NOT NULL,
  `LocalId` bigint(20) NOT NULL,
  `SpiresId` bigint(20) DEFAULT NULL,
  `SysValue` varchar(255) DEFAULT NULL,
  `Folded` varchar(255) DEFAULT NULL,
  `Comment` text,
  `Correction` text,
  `Condition` text,
  `SysName` varchar(255) DEFAULT NULL,
  `SysQN1` varchar(255) DEFAULT NULL,
  `SysQV1` varchar(255) DEFAULT NULL,
  `SysQN2` varchar(255) DEFAULT NULL,
  `SysQV2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PAPER_ID`,`LocalId`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `XAxes`
--

DROP TABLE IF EXISTS `XAxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `XAxes` (
  `AXIS_ID` bigint(20) NOT NULL,
  `Header` varchar(255) NOT NULL,
  `Unit` varchar(255) NOT NULL,
  `LocalId` int(11) NOT NULL,
  `_dataset_DATASET_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`AXIS_ID`),
  KEY `FK4F76F9D3BAA6EF` (`_dataset_DATASET_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `YAxes`
--

DROP TABLE IF EXISTS `YAxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `YAxes` (
  `AXIS_ID` bigint(20) NOT NULL,
  `Header` varchar(255) NOT NULL,
  `Unit` varchar(255) NOT NULL,
  `LocalId` int(11) NOT NULL,
  `Observable` varchar(255) DEFAULT NULL,
  `_dataset_DATASET_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`AXIS_ID`),
  KEY `FK505871E3BAA6EF` (`_dataset_DATASET_ID`),
  KEY `_dataset_DATASET_ID_index` (`_dataset_DATASET_ID`),
  KEY `AXIS_ID_index` (`AXIS_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `YAxisComments`
--

DROP TABLE IF EXISTS `YAxisComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `YAxisComments` (
  `AXIS_ID` bigint(20) NOT NULL,
  `Comments` text,
  `Posn` int(11) NOT NULL,
  PRIMARY KEY (`AXIS_ID`,`Posn`),
  KEY `FK57AB8FAE1D3D1D62` (`AXIS_ID`)
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--

--
-- Table structure for table `hibernate_sequences`
--

DROP TABLE IF EXISTS `hibernate_sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) DEFAULT NULL,
  `sequence_next_hi_value` int(11) DEFAULT NULL
) ENGINE=MyISAM;
/*!40101 SET character_set_client = @saved_cs_client */;

--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-01  1:00:12
