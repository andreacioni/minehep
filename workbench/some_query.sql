select *
from Papers;

select p.PAPER_ID, ds.DATASET_ID
from Papers p 
inner join Datasets ds on p.PAPER_ID = ds._paper_PAPER_ID;

select ds._paper_PAPER_ID, count(ds._paper_PAPER_ID)
from Datasets ds
group by ds._paper_PAPER_ID;

select *
from Papers p 
-- inner join Datasets ds on p.PAPER_ID = ds._paper_PAPER_ID
-- inner join Systematics syt on syt.PAPER_ID = p.PAPER_ID
-- inner join PaperExpts pex on pex.PAPER_ID = p.PAPER_ID
-- inner join PaperRefs prf on prf.PAPER_ID = p.PAPER_ID
inner join PaperComments pcm on pcm.PAPER_ID = p.PAPER_ID
where p.PAPER_ID = '9288'; /*89996 89997 89998*/

select *
from Papers p 
inner join Datasets ds on p.PAPER_ID = ds._paper_PAPER_ID
where p.Title like 'Search for supersymmetry in events with at least one photon%';

select *
from Datasets ds
where ds.DATASET_ID='89996';

select ds._paper_PAPER_ID, count(ds._paper_PAPER_ID) c,max(ds.localid) li
from Datasets ds
group by ds._paper_PAPER_ID having c <> li;

select *
from Datasets ds
-- inner join DatasetComments dsc on dsc.DATASET_ID = ds.DATASET_ID
-- inner join DsReactions dsr on dsr.DATASET_ID = ds.DATASET_ID
-- inner join DsPlabs dsp on dsp.DATASET_ID = ds.DATASET_ID
-- inner join DatasetErrors dse on dse.DATASET_ID = ds.DATASET_ID
-- inner join DsObservables dso on dso.DATASET_ID = ds.DATASET_ID
-- inner join DsKeywords dsk on dsk.DATASET_ID = ds.DATASET_ID
where ds.DATASET_ID='89996';

select *
from Datasets ds
inner join YAxes yax on yax._dataset_DATASET_ID = ds.DATASET_ID
-- inner join XAxes xax on xax._dataset_DATASET_ID = ds.DATASET_ID
-- inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
-- inner join Bins bns on bns._xAxis_AXIS_ID = xax.AXIS_ID

-- inner join PointErrors pte on pte.VALUE_ID = pts.VALUE_ID
where ds.DATASET_ID=89998
order by ds.DATASET_ID;

select *
from YAxes yax
-- inner join AxisReactions axr on axr._yAxis_AXIS_ID = yax.AXIS_ID
-- inner join InitialStates ins on ins.REACTION_ID = axr.REACTION_ID
-- inner join FinalStates fis on fis.REACTION_ID = axr.REACTION_ID
inner join AxisProperties axp on axp._yAxis_AXIS_ID = yax.AXIS_ID
inner join BaseProperties bpr on bpr.PROPERTY_ID = axp.PROPERTY_ID
where yax._dataset_DATASET_ID=89998;

select distinct dso.DsObservables
from DsObservables dso
union
select distinct yax.Observable
from YAxes yax;

select xax.AXIS_ID x, yax.AXIS_ID y
from Datasets ds
inner join YAxes yax on yax._dataset_DATASET_ID = ds.DATASET_ID
inner join XAxes xax on xax._dataset_DATASET_ID = ds.DATASET_ID
where ds.DATASET_ID=5872;

select xax.AXIS_ID x, count(xax.AXIS_ID)
from Datasets ds
inner join YAxes yax on yax._dataset_DATASET_ID = ds.DATASET_ID
inner join XAxes xax on xax._dataset_DATASET_ID = ds.DATASET_ID
group by x;

select yax.AXIS_ID y, count(yax.AXIS_ID)
from Datasets ds
inner join YAxes yax on yax._dataset_DATASET_ID = ds.DATASET_ID
inner join XAxes xax on xax._dataset_DATASET_ID = ds.DATASET_ID
group by y;

select yax._dataset_DATASET_ID
from YAxes yax
where yax.AXIS_ID = '9502740';

select ds._paper_PAPER_ID
from Datasets ds
where ds.DATASET_ID = '5872';

select max(level)
from DsKeywords;

/*  ============================================================================  */

select distinct Width
from Bins bin;

select distinct Relation, Header, HighValue, LowValue, Focus
from XAxes xax
inner join Bins bin on bin._xAxis_AXIS_ID = xax.AXIS_ID
where Header='E(P=1) IN GEV'
order by HighValue, LowValue, Focus;

select distinct Description, HighValue, LowValue, Focus
from Bins bin;

select * 
from Papers p
where p.Title like 'Differential branching fraction and angular moments analysis of the decay%';

select *
from AxisReactions axr
inner join YAxes yax  on axr._yAxis_AXIS_ID = yax.AXIS_ID ;

alter table AxisReactions drop key FK2DD7D8A9B9695FBE;
alter table AxisReactions drop key REACTION_ID_index;
alter table AxisReactions drop key _yAxis_AXIS_ID_index;

alter table AxisReactions add KEY `FK2DD7D8A9B9695FBE` (`_yAxis_AXIS_ID`);
alter table AxisReactions add KEY `_yAxis_AXIS_ID_index` (`_yAxis_AXIS_ID`);
alter table AxisReactions add KEY `REACTION_ID_index` (`REACTION_ID`);

select distinct Description from Bins;

select PAPER_ID, count(PAPER_ID) c, group_concat(element)
from PaperAuthors
group by PAPER_ID having c <> 1;


select distinct Header
from XAxes;

select *
from Papers p
inner join Keywords k on k.PAPER_ID = p.PAPER_ID
where p.PAPER_ID = '643';

select *
from BaseProperties bp
inner join AxisProperties ap on bp.PROPERTY_ID = ap.PROPERTY_ID;

select *
from XAxes ya
inner join AxisErrors ae on ya.AXIS_ID = ae._yAxis_AXIS_ID;

select *
from Points po
inner join PointErrors poe on po.VALUE_ID = poe.VALUE_ID;

select *
from Datasets ds
inner join DsPlabs dsp on ds.DATASET_ID = dsp.DATASET_ID;

select PAPER_ID,count(PAPER_ID)
from Systematics
where PAPER_ID is not null 
group by PAPER_ID;

select DATASET_ID,count(DATASET_ID) as a
from Datasets
where DATASET_ID is not null 
group by DATASET_ID having a <> 1;

select PAPER_ID,count(PAPER_ID) as a
from Papers
where PAPER_ID is not null 
group by PAPER_ID having a = 1;

select * from Datasets where DataType is not null;

select *
from Papers
inner join Systematics on Papers.PAPER_ID = Systematics.PAPER_ID