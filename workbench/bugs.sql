-- le reaction non corrispondono
select distinct Header, Value, Relation, PlusError, Name
FROM YAxisFacts yfa
inner join PaperDim ped on yfa.PAPER_ID = ped.ID
left join YErrorDim yed on yed.ID = yfa.POINT_ERROR_ID_1
left join ReactionsDim red on red.ID = yfa.REACTION_ID_3
where InspireId = 209441;
