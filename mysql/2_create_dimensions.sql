-- Star schema creation script

drop schema if exists minehep;
create schema minehep;

use minehep;

-- DIMENSIONs

-- Observables dimension (OK)
drop table if exists  ObservableDim;
create table ObservableDim (primary key (ID), unique(Name))
	select UUID() ID, Name
    from (
		select distinct Observable as Name from hepdata.YAxes
		where Observable is not null
		union
		select distinct DsObservables as Name from hepdata.DsObservables
		where DsObservables is not null
	) tmp;

/* 
	Qui abbiamo il problema che non gli Observable sono associati sia al Dataset che alla YAxis.
    C'è da notare che l'Observable associato all'asse ha un valore MOLTO MAGGIORE rispetto all'Observable
    che è associato tramite Dataset.
*/

-- XAxesDim dimension (OK)
drop table if exists XAxesDim;
create table XAxesDim (primary key (ID), unique(Header, Focus, FocusLength, HighValue, HighValueLength, LowValue, LowValueLength, Width))
	select UUID() ID, Header, Focus, FocusLength, HighValue, HighValueLength, LowValue, LowValueLength, Width
    from (
		select distinct Header, Focus, FocusLength, HighValue, HighValueLength, LowValue, LowValueLength, Width
		from hepdata.XAxes xax
        inner join hepdata.Bins bns on bns._xAxis_AXIS_ID = xax.AXIS_ID
    ) tmp;
    
-- PropertiesDim dimension (OK)
drop table if exists PropertiesDim;
create table PropertiesDim (primary key (ID))
	select UUID() ID, Name, Unit, Focus, HighValue, LowValue
	from (
		select distinct Name, Unit, Focus, HighValue, LowValue
		from hepdata.BaseProperties
    ) tmp;

-- mi servirà più avanti per collegare le properità alla tabella dei fatti con le nuove chiavi univoche
drop table if exists hepdata.temp_NewOldId_Properies;
create table hepdata.temp_NewOldId_Properies (primary key (PROPERTY_ID))
	select ID, PROPERTY_ID
	from PropertiesDim pd, hepdata.BaseProperties bps 
	where bps.Focus <=> pd.Focus and bps.HighValue <=> pd.HighValue and bps.LowValue <=> pd.LowValue and bps.Name <=> pd.Name and bps.Unit <=> pd.Unit;

-- PaperDim dimension (OK)

/*
	Qui abbiamo scelto l'anno più piccolo tra quelli che si possono ricondurre ad una certa Paper. Questo si è reso
    necessario perchè esistono Papers per le quali esistono più PaperRefs di anni diversi. In questo modo
    prendendo l'anno più piccolo abbiamo troviamo la prima pubblicazione del Paper. Con questa strategia però
    non tutti gli anni corrispondono a quelli che si possono individuare su hepdata.net .
*/

drop table if exists hepdata.temp_Papers;
create table hepdata.temp_Papers
		select pap.PAPER_ID PAPER_ID, InspireId, Title, DOI, ExptName, InformalName ExpInformalName, Lab ExpLab, min(Year) PubblicationYear
		from hepdata.Papers pap
        left join hepdata.PaperRefs prf on pap.PAPER_ID = prf.PAPER_ID -- relazione 1:N
        left join hepdata.PaperExpts pre on pap.PAPER_ID = pre.PAPER_ID -- relazione 1:1
        group by pap.PAPER_ID, InspireId, Title, DOI, ExptName ,InformalName, Lab; 

drop table if exists PaperDim;
create table PaperDim (primary key (ID), unique(InspireId), unique(DOI), unique(InspireId, DOI, ExptName, ExpInformalName, ExpLab, PubblicationYear))
	select UUID() ID, InspireId, Title, DOI, ExptName, ExpInformalName, ExpLab, PubblicationYear
    from hepdata.temp_Papers;

-- mi servirà più avanti per collegare le properità alla tabella dei fatti con le nuove chiavi univoche
drop table if exists hepdata.temp_NewOldId_Papers;
create table hepdata.temp_NewOldId_Papers (primary key (PAPER_ID))
	select ID, PAPER_ID
	from PaperDim pad, hepdata.temp_Papers pap 
	where pap.InspireId <=> pad.InspireId and pap.Title <=> pad.Title and pap.DOI <=> pad.DOI and pap.ExptName <=> pad.ExptName and pap.ExpInformalName <=> pad.ExpInformalName and pap.ExpLab <=> pad.ExpLab and pap.PubblicationYear <=> pad.PubblicationYear;

-- YErrorDim dimension (OK)
drop table if exists YErrorDim;
create table YErrorDim (primary key (ID), unique(MinusError, MinusErrorLength, Norm, PlusError, PlusErrorLength, Source))
	select distinct UUID() ID, Comment, MinusError, MinusErrorLength, Norm, PlusError, PlusErrorLength, Source
    from (
		select distinct Comment, MinusError, MinusErrorLength, Norm, PlusError, PlusErrorLength, Source
		from hepdata.AxisErrors
		union
		select distinct Comment, MinusError, MinusErrorLength, Norm, PlusError, PlusErrorLength, Source
		from hepdata.PointErrors
		union 
		select distinct Comment, MinusError, MinusErrorLength, Norm, PlusError, PlusErrorLength, Source
		from hepdata.DatasetErrors
    ) tmp;


-- ReactionStateDim dimension
drop table if exists ReactionStateDim;
create table ReactionStateDim (primary key (ID), unique(ParticleName, MultRelation, Multiplicity))
	select distinct UUID() ID, ParticleName, MultRelation, Multiplicity
    from (
		select distinct ParticleName, MultRelation, Multiplicity
		from hepdata.InitialStates
        union
        select distinct ParticleName, MultRelation, Multiplicity
		from hepdata.FinalStates
	) tmp;

-- mi servirà più avanti per collegare le reazioni alla tabella dei fatti con le nuove chiavi univoche
drop table if exists hepdata.temp_NewOldId_Reactions;
create table hepdata.temp_NewOldId_Reactions (primary key (ID), unique(REACTION_ID))
	select UUID() ID, REACTION_ID
	from (
		select distinct REACTION_ID
		from hepdata.AxisReactions axr
	) tmp;

drop table if exists hepdata.temp_NewOldId_YAxisReactions;
create table hepdata.temp_NewOldId_YAxisReactions (primary key (ID), unique(AXIS_ID))
	select UUID() ID, AXIS_ID
	from (
		select distinct _yAxis_AXIS_ID AXIS_ID 
		from hepdata.AxisReactions axr
	) tmp;



-- Reactions dimension
drop table if exists ReactionsDim;
create table ReactionsDim (primary key (ID), unique(Name))
	select distinct UUID() ID, Name
	from (
		select distinct DsReactions Name from hepdata.DsReactions
	) tmp;
    
-- Keywords dimension
drop table if exists KeywordsDim;
create table KeywordsDim (primary key (ID), unique(Name))
	select distinct UUID() ID, Name
	from (
		select distinct Keyword Name from hepdata.DsKeywords
	) tmp;
