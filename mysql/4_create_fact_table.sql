use minehep;

-- FACTS

drop table if exists  YAxisFacts;
create table YAxisFacts (primary key(ID))
	select 	UUID() ID, 
			lya.Header,
			pts.Value,
			pts.ValueLength,
			pts.Description,
			pts.Relation,
            lds.TableNumber,
			lxa.XAXIS_ID_1,
			lxa.XAXIS_ID_2,
			lxa.XAXIS_ID_3,
			lxa.XAXIS_ID_4,
			lxa.XAXIS_ID_5,
			lxa.XAXIS_ID_6,
			lxa.XAXIS_ID_7,
			rbt.BRIDGE_REACTION_ID,
			lre.REACTION_ID_1,
			lre.REACTION_ID_2,
			lre.REACTION_ID_3,
			lre.REACTION_ID_4,
			lre.REACTION_ID_5,
			lre.REACTION_ID_6,
			lre.REACTION_ID_7,
			lre.REACTION_ID_8,
			lre.REACTION_ID_9,
			lre.REACTION_ID_10,
			lre.REACTION_ID_11,
			lre.REACTION_ID_12,
			lre.REACTION_ID_13,
			lre.REACTION_ID_14,
			lre.REACTION_ID_15,
			lre.REACTION_ID_16,
			lre.REACTION_ID_17,
			lre.REACTION_ID_18,
			lre.REACTION_ID_19,
			lre.REACTION_ID_20,
			lre.REACTION_ID_21,
			lre.REACTION_ID_22,
			lre.REACTION_ID_23,
			lre.REACTION_ID_24,
			lre.REACTION_ID_25,
			lre.REACTION_ID_26,
			lre.REACTION_ID_27,
			lre.REACTION_ID_28,
			lre.REACTION_ID_29,
			lre.REACTION_ID_30,
			lre.REACTION_ID_31,
			lre.REACTION_ID_32,
			lre.REACTION_ID_33,
			lre.REACTION_ID_34,
			lre.REACTION_ID_35,
			lre.REACTION_ID_36,
			lre.REACTION_ID_37,
			lre.REACTION_ID_38,
			lre.REACTION_ID_39,
			lre.REACTION_ID_40,
			lre.REACTION_ID_41,
			lre.REACTION_ID_42,
			lre.REACTION_ID_43,
			lre.REACTION_ID_44,
			lre.REACTION_ID_45,
			lre.REACTION_ID_46,
			lre.REACTION_ID_47,
			lre.REACTION_ID_48,
			lre.REACTION_ID_49,
			lre.REACTION_ID_50,
			lre.REACTION_ID_51,
			lre.REACTION_ID_52,
			lre.REACTION_ID_53,
			lre.REACTION_ID_54,
			lre.REACTION_ID_55,
			lre.REACTION_ID_56,
			lre.REACTION_ID_57,
			lre.REACTION_ID_58,
			lre.REACTION_ID_59,
			lre.REACTION_ID_60,
			lre.REACTION_ID_61,
			lre.REACTION_ID_62,
			lre.REACTION_ID_63,
			lre.REACTION_ID_64,
			lre.REACTION_ID_65,
			lre.REACTION_ID_66,
			lre.REACTION_ID_67,
			lre.REACTION_ID_68,
			lre.REACTION_ID_69,
			lre.REACTION_ID_70,
			lre.REACTION_ID_71,
			lre.REACTION_ID_72,
			lre.REACTION_ID_73,
			lre.REACTION_ID_74,
			lre.REACTION_ID_75,
			lre.REACTION_ID_76,
			lre.REACTION_ID_77,
			lre.REACTION_ID_78,
			lre.REACTION_ID_79,
			lre.REACTION_ID_80,
			lre.REACTION_ID_81,
			lre.REACTION_ID_82,
			lre.REACTION_ID_83,
			lre.REACTION_ID_84,
			lre.REACTION_ID_85,
			lre.REACTION_ID_86,
			lre.REACTION_ID_87,
			lre.REACTION_ID_88,
			lre.REACTION_ID_89,
			lre.REACTION_ID_90,
			lre.REACTION_ID_91,
			lre.REACTION_ID_92,
			lre.REACTION_ID_93,
			lre.REACTION_ID_94,
			lre.REACTION_ID_95,
			lre.REACTION_ID_96,
			lre.REACTION_ID_97,
			lre.REACTION_ID_98,
			lre.REACTION_ID_99,
			lre.REACTION_ID_100,
			lre.REACTION_ID_101,
			lre.REACTION_ID_102,
			lre.REACTION_ID_103,
			lre.REACTION_ID_104,
			lre.REACTION_ID_105,
			lre.REACTION_ID_106,
			lre.REACTION_ID_107,
			lre.REACTION_ID_108,
			lre.REACTION_ID_109,
			lre.REACTION_ID_110,
			lre.REACTION_ID_111,
			lre.REACTION_ID_112,
			lre.REACTION_ID_113,
			lre.REACTION_ID_114,
			lre.REACTION_ID_115,
			lre.REACTION_ID_116,
			lre.REACTION_ID_117,
			lre.REACTION_ID_118,
			lre.REACTION_ID_119,
			lre.REACTION_ID_120,
			lre.REACTION_ID_121,
			lre.REACTION_ID_122,
			lre.REACTION_ID_123,
			lre.REACTION_ID_124,
			lre.REACTION_ID_125,
			lre.REACTION_ID_126,
			lre.REACTION_ID_127,
			lre.REACTION_ID_128,
			lre.REACTION_ID_129,
			lre.REACTION_ID_130,
			lre.REACTION_ID_131,
			lre.REACTION_ID_132,
			lre.REACTION_ID_133,
			lre.REACTION_ID_134,
			lre.REACTION_ID_135,
			lre.REACTION_ID_136,
			lre.REACTION_ID_137,
			lre.REACTION_ID_138,
			lre.REACTION_ID_139,
			lre.REACTION_ID_140,
			lre.REACTION_ID_141,
			lre.REACTION_ID_142,
			lre.REACTION_ID_143,
			lre.REACTION_ID_144,
			lre.REACTION_ID_145,
			lre.REACTION_ID_146,
			lre.REACTION_ID_147,
			lre.REACTION_ID_148,
			lre.REACTION_ID_149,
			lre.REACTION_ID_150,
			lre.REACTION_ID_151,
			lre.REACTION_ID_152,
			lre.REACTION_ID_153,
			lre.REACTION_ID_154,
			lre.REACTION_ID_155,
			lre.REACTION_ID_156,
			lre.REACTION_ID_157,
			lre.REACTION_ID_158,
			lre.REACTION_ID_159,
			lre.REACTION_ID_160,
			lpr.PROPERTY_ID_1,
			lpr.PROPERTY_ID_2,
			lpr.PROPERTY_ID_3,
			lpr.PROPERTY_ID_4,
			lpr.PROPERTY_ID_5,
			lpr.PROPERTY_ID_6,
			lpr.PROPERTY_ID_7,
			lpr.PROPERTY_ID_8,
			lpr.PROPERTY_ID_9,
			lpr.PROPERTY_ID_10,
			lpr.PROPERTY_ID_11,
			lpr.PROPERTY_ID_12,
			lpr.PROPERTY_ID_13,
			lpr.PROPERTY_ID_14,
			lpr.PROPERTY_ID_15,
			lpr.PROPERTY_ID_16,
			lpr.PROPERTY_ID_17,
			lpr.PROPERTY_ID_18,
			lpr.PROPERTY_ID_19,
			lpr.PROPERTY_ID_20,
			lpr.PROPERTY_ID_21,
			lpr.PROPERTY_ID_22,
			lpr.PROPERTY_ID_23,
			lpr.PROPERTY_ID_24,
			lpr.PROPERTY_ID_25,
			lpr.PROPERTY_ID_26,
			lpr.PROPERTY_ID_27,
			lpr.PROPERTY_ID_28,
			lpr.PROPERTY_ID_29,
			lpr.PROPERTY_ID_30,
			lpr.PROPERTY_ID_31,
			lpr.PROPERTY_ID_32,
			lpr.PROPERTY_ID_33,
			lpr.PROPERTY_ID_34,
			lpr.PROPERTY_ID_35,
			lpa.PAPER_ID,
			ler.POINT_ERROR_ID_1,
			ler.POINT_ERROR_ID_2,
			ler.POINT_ERROR_ID_3,
			ler.POINT_ERROR_ID_4,
			ler.POINT_ERROR_ID_5,
			ler.POINT_ERROR_ID_6,
			ler.POINT_ERROR_ID_7,
			ler.POINT_ERROR_ID_8,
			ler.POINT_ERROR_ID_9,
			ler.POINT_ERROR_ID_10,
			ler.POINT_ERROR_ID_11,
			ler.POINT_ERROR_ID_12,
			ler.POINT_ERROR_ID_13,
			ler.POINT_ERROR_ID_14,
			ler.POINT_ERROR_ID_15,
			ler.POINT_ERROR_ID_16,
			ler.POINT_ERROR_ID_17,
			ler.POINT_ERROR_ID_18,
			ler.POINT_ERROR_ID_19,
			ler.POINT_ERROR_ID_20,
			ler.POINT_ERROR_ID_21,
			ler.POINT_ERROR_ID_22,
			ler.POINT_ERROR_ID_23,
			ler.POINT_ERROR_ID_24,
			ler.POINT_ERROR_ID_25,
			ler.POINT_ERROR_ID_26,
			ler.POINT_ERROR_ID_27,
			ler.POINT_ERROR_ID_28,
			ler.POINT_ERROR_ID_29,
			ler.POINT_ERROR_ID_30,
			ler.POINT_ERROR_ID_31,
			ler.POINT_ERROR_ID_32,
			ler.POINT_ERROR_ID_33,
			ler.POINT_ERROR_ID_34,
			ler.POINT_ERROR_ID_35,
			ler.POINT_ERROR_ID_36,
			ler.POINT_ERROR_ID_37,
			ler.POINT_ERROR_ID_38,
			ler.POINT_ERROR_ID_39,
			ler.POINT_ERROR_ID_40,
			ler.POINT_ERROR_ID_41,
			ler.POINT_ERROR_ID_42,
			ler.POINT_ERROR_ID_43,
			ler.POINT_ERROR_ID_44,
			ler.POINT_ERROR_ID_45,
			ler.POINT_ERROR_ID_46,
			ler.POINT_ERROR_ID_47,
			ler.POINT_ERROR_ID_48,
			ler.POINT_ERROR_ID_49,
			ler.POINT_ERROR_ID_50,
			ler.POINT_ERROR_ID_51,
			ler.POINT_ERROR_ID_52,
			ler.POINT_ERROR_ID_53,
			ler.POINT_ERROR_ID_54,
			ler.POINT_ERROR_ID_55,
			ler.POINT_ERROR_ID_56,
			ler.POINT_ERROR_ID_57,
			ler.POINT_ERROR_ID_58,
			ler.POINT_ERROR_ID_59,
			ler.POINT_ERROR_ID_60,
			ler.POINT_ERROR_ID_61,
			ler.POINT_ERROR_ID_62,
			ler.POINT_ERROR_ID_63,
			ler.POINT_ERROR_ID_64,
			ler.POINT_ERROR_ID_65,
			ler.POINT_ERROR_ID_66,
			ler.POINT_ERROR_ID_67,
			ler.POINT_ERROR_ID_68,
			ler.POINT_ERROR_ID_69,
			ler.POINT_ERROR_ID_70,
			ler.POINT_ERROR_ID_71,
			ler.AXIS_ERROR_ID_1,
			ler.AXIS_ERROR_ID_2,
			ler.AXIS_ERROR_ID_3,
			ler.AXIS_ERROR_ID_4,
			ler.AXIS_ERROR_ID_5,
			ler.AXIS_ERROR_ID_6,
			ler.AXIS_ERROR_ID_7,
			ler.DATASET_ERROR_ID_1,
			ler.DATASET_ERROR_ID_2,
			ler.DATASET_ERROR_ID_3,
			ler.DATASET_ERROR_ID_4,
			ler.DATASET_ERROR_ID_5,
			ler.DATASET_ERROR_ID_6,
			ler.DATASET_ERROR_ID_7,
			ler.DATASET_ERROR_ID_8,
			ler.DATASET_ERROR_ID_9,
			ler.DATASET_ERROR_ID_10,
			ler.DATASET_ERROR_ID_11,
			ler.DATASET_ERROR_ID_12,
			ler.DATASET_ERROR_ID_13,
			ler.DATASET_ERROR_ID_14,
			ler.DATASET_ERROR_ID_15,
			ler.DATASET_ERROR_ID_16,
			ler.DATASET_ERROR_ID_17,
			ler.DATASET_ERROR_ID_18,
			ler.DATASET_ERROR_ID_19,
			lob.AXIS_OBSERVABLE_ID,
			lob.DATASET_OBSERVABLE_ID_1,
			lob.DATASET_OBSERVABLE_ID_2,
			lob.DATASET_OBSERVABLE_ID_3,
			lob.DATASET_OBSERVABLE_ID_4,
			lob.DATASET_OBSERVABLE_ID_5,
			lob.DATASET_OBSERVABLE_ID_6,
			lob.DATASET_OBSERVABLE_ID_7,
			lob.DATASET_OBSERVABLE_ID_8,
			lob.DATASET_OBSERVABLE_ID_9,
			lob.DATASET_OBSERVABLE_ID_10,
			lob.DATASET_OBSERVABLE_ID_11,
			lks.KEYWORD_ID_1,
			lks.KEYWORD_ID_2,
			lks.KEYWORD_ID_3,
			lks.KEYWORD_ID_4,
			lks.KEYWORD_ID_5,
			lks.KEYWORD_ID_6,
			lks.KEYWORD_ID_7,
			lks.KEYWORD_ID_8,
			lks.KEYWORD_ID_9,
			lks.KEYWORD_ID_10,
			lks.KEYWORD_ID_11,
			lks.KEYWORD_ID_12,
			lks.KEYWORD_ID_13,
			lks.KEYWORD_ID_14
	from hepdata.Points pts
    inner join hepdata.temp_Link_YAxes lya on lya.VALUE_ID = pts.VALUE_ID
    inner join hepdata.temp_Link_Datasets lds on lds.VALUE_ID = pts.VALUE_ID
	left join hepdata.temp_Link_XAxes lxa on lxa.VALUE_ID = pts.VALUE_ID
	left join hepdata.temp_Link_Reactions lre on lre.VALUE_ID = pts.VALUE_ID
	left join hepdata.temp_Link_Properties lpr on lpr.VALUE_ID = pts.VALUE_ID
	left join hepdata.temp_Link_Papers lpa on lpa.VALUE_ID = pts.VALUE_ID
	left join hepdata.temp_Link_Errors ler on ler.VALUE_ID = pts.VALUE_ID
	left join hepdata.temp_Link_Observable lob on lob.VALUE_ID = pts.VALUE_ID
	left join hepdata.temp_Link_Keywords lks on lks.VALUE_ID = pts.VALUE_ID
	left join hepdata.temp_Link_BridgeReactions rbt on rbt.VALUE_ID = pts.VALUE_ID;