use minehep;

drop table if exists ReactionStateBridge;
create table ReactionStateBridge (unique (REACTION_GROUP_ID, LOCAL_REACTION_ID, InitialFinalFlag, REACTION_STATE_ID))
	select NEW_AXIS_ID REACTION_GROUP_ID, NEW_REACTION_ID LOCAL_REACTION_ID, InitialFinalFlag, REACTION_STATE_ID
    from (
		select NEW_AXIS_ID, NEW_REACTION_ID, InitialFinalFlag, REACTION_STATE_ID
		from hepdata.temp_NewOld_InitialStates noi
		union
		select NEW_AXIS_ID, NEW_REACTION_ID, InitialFinalFlag, REACTION_STATE_ID
		from hepdata.temp_NewOld_FinalStates nof
	) tmp order by REACTION_GROUP_ID, LOCAL_REACTION_ID, InitialFinalFlag