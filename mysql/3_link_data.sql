use hepdata;

-- Datasets <------> Y 
drop table if exists temp_Link_Datasets;
create table temp_Link_Datasets (primary key (VALUE_ID))
	select distinct	pts.VALUE_ID, dst.LocalId TableNumber
	from Datasets dst
    inner join YAxes yax on yax._dataset_DATASET_ID = dst.DATASET_ID
	inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID;

-- Y <------> YAxes 
drop table if exists temp_Link_YAxes;
create table temp_Link_YAxes (primary key (VALUE_ID))
	select distinct	pts.VALUE_ID, Header
	from YAxes yax
	inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID;

-- Y <------> X 

drop table if exists temp_RowPerGroup_XAxes;
create table temp_RowPerGroup_XAxes (primary key (VALUE_ID, ID))
	 select @row_number := case when @vid = VALUE_ID then @row_number + 1 else 1 end as ROW_NUM,
			@vid := VALUE_ID as VALUE_ID,
			ID
	 from (
		select distinct pts.VALUE_ID, ID
		from XAxes xax
		inner join Bins bns on bns._xAxis_AXIS_ID = xax.AXIS_ID 
		inner join YAxes yax on yax._dataset_DATASET_ID = xax._dataset_DATASET_ID 
		inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID and bns.LocalId = pts.LocalId
        inner join minehep.XAxesDim xad on xad.Header <=> xax.Header and xad.Focus <=> bns.Focus and xad.FocusLength <=> bns.FocusLength and xad.HighValue <=> bns.HighValue and xad.HighValueLength <=> bns.HighValueLength and xad.LowValue <=> bns.LowValue and xad.LowValueLength <=> bns.LowValueLength and xad.Width <=> bns.Width
		order by pts.VALUE_ID
	) tmp;

drop table if exists temp_Link_XAxes;
create table temp_Link_XAxes (primary key (VALUE_ID))
	select
		VALUE_ID,
		max(case when ROW_NUM=1 then ID else null end) as XAXIS_ID_1,
		max(case when ROW_NUM=2 then ID else null end) as XAXIS_ID_2,
		max(case when ROW_NUM=3 then ID else null end) as XAXIS_ID_3,
		max(case when ROW_NUM=4 then ID else null end) as XAXIS_ID_4,
		max(case when ROW_NUM=5 then ID else null end) as XAXIS_ID_5,
		max(case when ROW_NUM=6 then ID else null end) as XAXIS_ID_6,
		max(case when ROW_NUM=7 then ID else null end) as XAXIS_ID_7
	from temp_RowPerGroup_XAxes rpx
	group by VALUE_ID;
    
-- Y <------> AxisReaction

-- creo una tabela simile a quella delle AxisReactions che contiene però i nuovi e vecchi ID                                                                                        inze AxisReactions
drop table if exists temp_NewOld_AxisReactions;
create table temp_NewOld_AxisReactions (unique(OLD_REACTION_ID, OLD_AXIS_ID), unique(NEW_REACTION_ID, NEW_AXIS_ID))
	select axr.REACTION_ID OLD_REACTION_ID, nor.ID NEW_REACTION_ID, axr._yAxis_AXIS_ID OLD_AXIS_ID, noy.ID NEW_AXIS_ID
	from AxisReactions axr
	inner join temp_NewOldId_Reactions nor on nor.REACTION_ID = axr.REACTION_ID
	inner join temp_NewOldId_YAxisReactions noy on noy.AXIS_ID = axr._yAxis_AXIS_ID;
    
-- metto in join la precdente tabella con la IntialState
drop table if exists temp_NewOld_InitialStates;
create table temp_NewOld_InitialStates (unique(NEW_AXIS_ID, NEW_REACTION_ID, REACTION_STATE_ID))
	select tna.NEW_AXIS_ID, tna.NEW_REACTION_ID, InitialFinalFlag, rsd.ID REACTION_STATE_ID
    from temp_NewOld_AxisReactions tna
    inner join InitialStates ins on tna.OLD_REACTION_ID = ins.REACTION_ID
    inner join minehep.ReactionStateDim rsd on rsd.ParticleName <=> ins.ParticleName and rsd.MultRelation <=> ins.MultRelation and rsd.Multiplicity = ins.Multiplicity;

-- metto in join la precedente tabella con la FinalStates
drop table if exists temp_NewOld_FinalStates;
create table temp_NewOld_FinalStates (unique(NEW_AXIS_ID, NEW_REACTION_ID, REACTION_STATE_ID))
	select tna.NEW_AXIS_ID, tna.NEW_REACTION_ID, InitialFinalFlag, rsd.ID REACTION_STATE_ID
    from temp_NewOld_AxisReactions tna
    inner join FinalStates fns on tna.OLD_REACTION_ID = fns.REACTION_ID
    inner join minehep.ReactionStateDim rsd on rsd.ParticleName <=> fns.ParticleName and rsd.MultRelation <=> fns.MultRelation and rsd.Multiplicity = fns.Multiplicity;

-- faccio la tabella di link tra la bridge table e la tabella dei fatti
drop table if exists temp_Link_BridgeReactions;
create table temp_Link_BridgeReactions (primary key (VALUE_ID))
	select distinct VALUE_ID, NEW_AXIS_ID BRIDGE_REACTION_ID
    from temp_NewOld_AxisReactions tna
    inner join Points pts on pts._yAxis_AXIS_ID = tna.OLD_AXIS_ID;

-- Y <------> ReactionsDim (NOT TESTED)

drop table if exists temp_RowPerGroup_Reactions;
create table temp_RowPerGroup_Reactions (primary key (VALUE_ID, ID))
	 select @row_number := case when @vid = VALUE_ID then @row_number + 1 else 1 end as ROW_NUM,
			@vid := VALUE_ID as VALUE_ID,
			ID
	 from (
		select distinct VALUE_ID, ID
		from YAxes yax
		inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
		inner join DsReactions dsr on yax._dataset_DATASET_ID = dsr.DATASET_ID
		inner join minehep.ReactionsDim red on red.Name = dsr.DsReactions
		order by VALUE_ID
	 ) rmp;

drop table if exists temp_Link_Reactions;
create table temp_Link_Reactions (primary key (VALUE_ID))
	select
		VALUE_ID,
		max(case when ROW_NUM=1  then ID else null end) as REACTION_ID_1,
		max(case when ROW_NUM=2  then ID else null end) as REACTION_ID_2,
		max(case when ROW_NUM=3  then ID else null end) as REACTION_ID_3,
		max(case when ROW_NUM=4  then ID else null end) as REACTION_ID_4,
		max(case when ROW_NUM=5  then ID else null end) as REACTION_ID_5,
		max(case when ROW_NUM=6  then ID else null end) as REACTION_ID_6,
		max(case when ROW_NUM=7  then ID else null end) as REACTION_ID_7,
		max(case when ROW_NUM=8  then ID else null end) as REACTION_ID_8,
		max(case when ROW_NUM=9  then ID else null end) as REACTION_ID_9,
		max(case when ROW_NUM=10 then ID else null end) as REACTION_ID_10,
		max(case when ROW_NUM=11 then ID else null end) as REACTION_ID_11,
		max(case when ROW_NUM=12 then ID else null end) as REACTION_ID_12,
		max(case when ROW_NUM=13 then ID else null end) as REACTION_ID_13,
		max(case when ROW_NUM=14 then ID else null end) as REACTION_ID_14,
		max(case when ROW_NUM=15 then ID else null end) as REACTION_ID_15,
		max(case when ROW_NUM=16 then ID else null end) as REACTION_ID_16,
		max(case when ROW_NUM=17 then ID else null end) as REACTION_ID_17,
		max(case when ROW_NUM=18 then ID else null end) as REACTION_ID_18,
		max(case when ROW_NUM=19 then ID else null end) as REACTION_ID_19,
		max(case when ROW_NUM=20 then ID else null end) as REACTION_ID_20,
		max(case when ROW_NUM=21 then ID else null end) as REACTION_ID_21,
		max(case when ROW_NUM=22 then ID else null end) as REACTION_ID_22,
		max(case when ROW_NUM=23 then ID else null end) as REACTION_ID_23,
		max(case when ROW_NUM=24 then ID else null end) as REACTION_ID_24,
		max(case when ROW_NUM=25 then ID else null end) as REACTION_ID_25,
		max(case when ROW_NUM=26 then ID else null end) as REACTION_ID_26,
		max(case when ROW_NUM=27 then ID else null end) as REACTION_ID_27,
		max(case when ROW_NUM=28 then ID else null end) as REACTION_ID_28,
		max(case when ROW_NUM=29 then ID else null end) as REACTION_ID_29,
		max(case when ROW_NUM=30 then ID else null end) as REACTION_ID_30,
		max(case when ROW_NUM=31 then ID else null end) as REACTION_ID_31,
		max(case when ROW_NUM=32 then ID else null end) as REACTION_ID_32,
		max(case when ROW_NUM=33 then ID else null end) as REACTION_ID_33,
		max(case when ROW_NUM=34 then ID else null end) as REACTION_ID_34,
		max(case when ROW_NUM=35 then ID else null end) as REACTION_ID_35,
		max(case when ROW_NUM=36 then ID else null end) as REACTION_ID_36,
		max(case when ROW_NUM=37 then ID else null end) as REACTION_ID_37,
		max(case when ROW_NUM=38 then ID else null end) as REACTION_ID_38,
		max(case when ROW_NUM=39 then ID else null end) as REACTION_ID_39,
		max(case when ROW_NUM=40 then ID else null end) as REACTION_ID_40,
		max(case when ROW_NUM=41 then ID else null end) as REACTION_ID_41,
		max(case when ROW_NUM=42 then ID else null end) as REACTION_ID_42,
		max(case when ROW_NUM=43 then ID else null end) as REACTION_ID_43,
		max(case when ROW_NUM=44 then ID else null end) as REACTION_ID_44,
		max(case when ROW_NUM=45 then ID else null end) as REACTION_ID_45,
		max(case when ROW_NUM=46 then ID else null end) as REACTION_ID_46,
		max(case when ROW_NUM=47 then ID else null end) as REACTION_ID_47,
		max(case when ROW_NUM=48 then ID else null end) as REACTION_ID_48,
		max(case when ROW_NUM=49 then ID else null end) as REACTION_ID_49,
		max(case when ROW_NUM=50 then ID else null end) as REACTION_ID_50,
		max(case when ROW_NUM=51 then ID else null end) as REACTION_ID_51,
		max(case when ROW_NUM=52 then ID else null end) as REACTION_ID_52,
		max(case when ROW_NUM=53 then ID else null end) as REACTION_ID_53,
		max(case when ROW_NUM=54 then ID else null end) as REACTION_ID_54,
		max(case when ROW_NUM=55 then ID else null end) as REACTION_ID_55,
		max(case when ROW_NUM=56 then ID else null end) as REACTION_ID_56,
		max(case when ROW_NUM=57 then ID else null end) as REACTION_ID_57,
		max(case when ROW_NUM=58 then ID else null end) as REACTION_ID_58,
		max(case when ROW_NUM=59 then ID else null end) as REACTION_ID_59,
		max(case when ROW_NUM=60 then ID else null end) as REACTION_ID_60,
		max(case when ROW_NUM=61 then ID else null end) as REACTION_ID_61,
		max(case when ROW_NUM=62 then ID else null end) as REACTION_ID_62,
		max(case when ROW_NUM=63 then ID else null end) as REACTION_ID_63,
		max(case when ROW_NUM=64 then ID else null end) as REACTION_ID_64,
		max(case when ROW_NUM=65 then ID else null end) as REACTION_ID_65,
		max(case when ROW_NUM=66 then ID else null end) as REACTION_ID_66,
		max(case when ROW_NUM=67 then ID else null end) as REACTION_ID_67,
		max(case when ROW_NUM=68 then ID else null end) as REACTION_ID_68,
		max(case when ROW_NUM=69 then ID else null end) as REACTION_ID_69,
		max(case when ROW_NUM=70 then ID else null end) as REACTION_ID_70,
		max(case when ROW_NUM=71 then ID else null end) as REACTION_ID_71,
		max(case when ROW_NUM=72 then ID else null end) as REACTION_ID_72,
		max(case when ROW_NUM=73 then ID else null end) as REACTION_ID_73,
		max(case when ROW_NUM=74 then ID else null end) as REACTION_ID_74,
		max(case when ROW_NUM=75 then ID else null end) as REACTION_ID_75,
		max(case when ROW_NUM=76 then ID else null end) as REACTION_ID_76,
		max(case when ROW_NUM=77 then ID else null end) as REACTION_ID_77,
		max(case when ROW_NUM=78 then ID else null end) as REACTION_ID_78,
		max(case when ROW_NUM=79 then ID else null end) as REACTION_ID_79,
		max(case when ROW_NUM=80 then ID else null end) as REACTION_ID_80,
		max(case when ROW_NUM=81 then ID else null end) as REACTION_ID_81,
		max(case when ROW_NUM=82 then ID else null end) as REACTION_ID_82,
		max(case when ROW_NUM=83 then ID else null end) as REACTION_ID_83,
		max(case when ROW_NUM=84 then ID else null end) as REACTION_ID_84,
		max(case when ROW_NUM=85 then ID else null end) as REACTION_ID_85,
		max(case when ROW_NUM=86 then ID else null end) as REACTION_ID_86,
		max(case when ROW_NUM=87 then ID else null end) as REACTION_ID_87,
		max(case when ROW_NUM=88 then ID else null end) as REACTION_ID_88,
		max(case when ROW_NUM=89 then ID else null end) as REACTION_ID_89,
		max(case when ROW_NUM=90 then ID else null end) as REACTION_ID_90,
		max(case when ROW_NUM=91 then ID else null end) as REACTION_ID_91,
		max(case when ROW_NUM=92 then ID else null end) as REACTION_ID_92,
		max(case when ROW_NUM=93 then ID else null end) as REACTION_ID_93,
		max(case when ROW_NUM=94 then ID else null end) as REACTION_ID_94,
		max(case when ROW_NUM=95 then ID else null end) as REACTION_ID_95,
		max(case when ROW_NUM=96 then ID else null end) as REACTION_ID_96,
		max(case when ROW_NUM=97 then ID else null end) as REACTION_ID_97,
		max(case when ROW_NUM=98 then ID else null end) as REACTION_ID_98,
		max(case when ROW_NUM=99 then ID else null end) as REACTION_ID_99,
		max(case when ROW_NUM=100 then ID else null end) as REACTION_ID_100,
		max(case when ROW_NUM=101 then ID else null end) as REACTION_ID_101,
		max(case when ROW_NUM=102 then ID else null end) as REACTION_ID_102,
		max(case when ROW_NUM=103 then ID else null end) as REACTION_ID_103,
		max(case when ROW_NUM=104 then ID else null end) as REACTION_ID_104,
		max(case when ROW_NUM=105 then ID else null end) as REACTION_ID_105,
		max(case when ROW_NUM=106 then ID else null end) as REACTION_ID_106,
		max(case when ROW_NUM=107 then ID else null end) as REACTION_ID_107,
		max(case when ROW_NUM=108 then ID else null end) as REACTION_ID_108,
		max(case when ROW_NUM=109 then ID else null end) as REACTION_ID_109,
		max(case when ROW_NUM=110 then ID else null end) as REACTION_ID_110,
		max(case when ROW_NUM=111 then ID else null end) as REACTION_ID_111,
		max(case when ROW_NUM=112 then ID else null end) as REACTION_ID_112,
		max(case when ROW_NUM=113 then ID else null end) as REACTION_ID_113,
		max(case when ROW_NUM=114 then ID else null end) as REACTION_ID_114,
		max(case when ROW_NUM=115 then ID else null end) as REACTION_ID_115,
		max(case when ROW_NUM=116 then ID else null end) as REACTION_ID_116,
		max(case when ROW_NUM=117 then ID else null end) as REACTION_ID_117,
		max(case when ROW_NUM=118 then ID else null end) as REACTION_ID_118,
		max(case when ROW_NUM=119 then ID else null end) as REACTION_ID_119,
		max(case when ROW_NUM=120 then ID else null end) as REACTION_ID_120,
		max(case when ROW_NUM=121 then ID else null end) as REACTION_ID_121,
		max(case when ROW_NUM=122 then ID else null end) as REACTION_ID_122,
		max(case when ROW_NUM=123 then ID else null end) as REACTION_ID_123,
		max(case when ROW_NUM=124 then ID else null end) as REACTION_ID_124,
		max(case when ROW_NUM=125 then ID else null end) as REACTION_ID_125,
		max(case when ROW_NUM=126 then ID else null end) as REACTION_ID_126,
		max(case when ROW_NUM=127 then ID else null end) as REACTION_ID_127,
		max(case when ROW_NUM=128 then ID else null end) as REACTION_ID_128,
		max(case when ROW_NUM=129 then ID else null end) as REACTION_ID_129,
		max(case when ROW_NUM=130 then ID else null end) as REACTION_ID_130,
		max(case when ROW_NUM=131 then ID else null end) as REACTION_ID_131,
		max(case when ROW_NUM=132 then ID else null end) as REACTION_ID_132,
		max(case when ROW_NUM=133 then ID else null end) as REACTION_ID_133,
		max(case when ROW_NUM=134 then ID else null end) as REACTION_ID_134,
		max(case when ROW_NUM=135 then ID else null end) as REACTION_ID_135,
		max(case when ROW_NUM=136 then ID else null end) as REACTION_ID_136,
		max(case when ROW_NUM=137 then ID else null end) as REACTION_ID_137,
		max(case when ROW_NUM=138 then ID else null end) as REACTION_ID_138,
		max(case when ROW_NUM=139 then ID else null end) as REACTION_ID_139,
		max(case when ROW_NUM=140 then ID else null end) as REACTION_ID_140,
		max(case when ROW_NUM=141 then ID else null end) as REACTION_ID_141,
		max(case when ROW_NUM=142 then ID else null end) as REACTION_ID_142,
		max(case when ROW_NUM=143 then ID else null end) as REACTION_ID_143,
		max(case when ROW_NUM=144 then ID else null end) as REACTION_ID_144,
		max(case when ROW_NUM=145 then ID else null end) as REACTION_ID_145,
		max(case when ROW_NUM=146 then ID else null end) as REACTION_ID_146,
		max(case when ROW_NUM=147 then ID else null end) as REACTION_ID_147,
		max(case when ROW_NUM=148 then ID else null end) as REACTION_ID_148,
		max(case when ROW_NUM=149 then ID else null end) as REACTION_ID_149,
		max(case when ROW_NUM=150 then ID else null end) as REACTION_ID_150,
		max(case when ROW_NUM=151 then ID else null end) as REACTION_ID_151,
		max(case when ROW_NUM=152 then ID else null end) as REACTION_ID_152,
		max(case when ROW_NUM=153 then ID else null end) as REACTION_ID_153,
		max(case when ROW_NUM=154 then ID else null end) as REACTION_ID_154,
		max(case when ROW_NUM=155 then ID else null end) as REACTION_ID_155,
		max(case when ROW_NUM=156 then ID else null end) as REACTION_ID_156,
		max(case when ROW_NUM=157 then ID else null end) as REACTION_ID_157,
		max(case when ROW_NUM=158 then ID else null end) as REACTION_ID_158,
		max(case when ROW_NUM=159 then ID else null end) as REACTION_ID_159,
		max(case when ROW_NUM=160 then ID else null end) as REACTION_ID_160
	from temp_RowPerGroup_Reactions trr
	group by VALUE_ID;


-- Y <------> Properties (OK)

-- creo una tabella dove associo ad ogni property di un certo VALUE_ID un indice che parte da 1 che poi utilizzerò nella query successiva
drop table if exists temp_RowPerGroup_Properties;
create table temp_RowPerGroup_Properties (primary key (VALUE_ID, PROPERTY_ID, ID))
	 select @row_number := case when @vid = VALUE_ID then @row_number + 1 else 1 end as ROW_NUM,
			@vid := VALUE_ID as VALUE_ID, 
			PROPERTY_ID, -- TODO non mi serve?
			ID
	 from (
		select 	pts.VALUE_ID, 
				bps.PROPERTY_ID,
				tnp.ID
	   from YAxes yax
	   inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
	   inner join AxisProperties axp on yax.AXIS_ID = axp._yAxis_AXIS_ID
	   inner join BaseProperties bps on bps.PROPERTY_ID = axp.PROPERTY_ID
	   inner join temp_NewOldId_Properies tnp on bps.PROPERTY_ID = tnp.PROPERTY_ID
	   order by pts.VALUE_ID
	 ) rmp;

drop table if exists temp_Link_Properties;
create table temp_Link_Properties (primary key (VALUE_ID))
 select
  VALUE_ID,
  max(case when ROW_NUM = 1  then trp.ID else null end) as PROPERTY_ID_1,
  max(case when ROW_NUM = 2  then trp.ID else null end) as PROPERTY_ID_2,
  max(case when ROW_NUM = 3  then trp.ID else null end) as PROPERTY_ID_3,
  max(case when ROW_NUM = 4  then trp.ID else null end) as PROPERTY_ID_4,
  max(case when ROW_NUM = 5  then trp.ID else null end) as PROPERTY_ID_5,
  max(case when ROW_NUM = 6  then trp.ID else null end) as PROPERTY_ID_6,
  max(case when ROW_NUM = 7  then trp.ID else null end) as PROPERTY_ID_7,
  max(case when ROW_NUM = 8  then trp.ID else null end) as PROPERTY_ID_8,
  max(case when ROW_NUM = 9  then trp.ID else null end) as PROPERTY_ID_9,
  max(case when ROW_NUM = 10 then trp.ID else null end) as PROPERTY_ID_10,
  max(case when ROW_NUM = 11 then trp.ID else null end) as PROPERTY_ID_11,
  max(case when ROW_NUM = 12 then trp.ID else null end) as PROPERTY_ID_12,
  max(case when ROW_NUM = 13 then trp.ID else null end) as PROPERTY_ID_13,
  max(case when ROW_NUM = 14 then trp.ID else null end) as PROPERTY_ID_14,
  max(case when ROW_NUM = 15 then trp.ID else null end) as PROPERTY_ID_15,
  max(case when ROW_NUM = 16 then trp.ID else null end) as PROPERTY_ID_16,
  max(case when ROW_NUM = 17 then trp.ID else null end) as PROPERTY_ID_17,
  max(case when ROW_NUM = 18 then trp.ID else null end) as PROPERTY_ID_18,
  max(case when ROW_NUM = 19 then trp.ID else null end) as PROPERTY_ID_19,
  max(case when ROW_NUM = 20 then trp.ID else null end) as PROPERTY_ID_20,
  max(case when ROW_NUM = 21 then trp.ID else null end) as PROPERTY_ID_21,
  max(case when ROW_NUM = 22 then trp.ID else null end) as PROPERTY_ID_22,
  max(case when ROW_NUM = 23 then trp.ID else null end) as PROPERTY_ID_23,
  max(case when ROW_NUM = 24 then trp.ID else null end) as PROPERTY_ID_24,
  max(case when ROW_NUM = 25 then trp.ID else null end) as PROPERTY_ID_25,
  max(case when ROW_NUM = 26 then trp.ID else null end) as PROPERTY_ID_26,
  max(case when ROW_NUM = 27 then trp.ID else null end) as PROPERTY_ID_27,
  max(case when ROW_NUM = 28 then trp.ID else null end) as PROPERTY_ID_28,
  max(case when ROW_NUM = 29 then trp.ID else null end) as PROPERTY_ID_29,
  max(case when ROW_NUM = 30 then trp.ID else null end) as PROPERTY_ID_30,
  max(case when ROW_NUM = 31 then trp.ID else null end) as PROPERTY_ID_31,
  max(case when ROW_NUM = 32 then trp.ID else null end) as PROPERTY_ID_32,
  max(case when ROW_NUM = 33 then trp.ID else null end) as PROPERTY_ID_33,
  max(case when ROW_NUM = 34 then trp.ID else null end) as PROPERTY_ID_34,
  max(case when ROW_NUM = 35 then trp.ID else null end) as PROPERTY_ID_35
 from temp_RowPerGroup_Properties trp
 group by VALUE_ID;
 
 -- Y <------> Papers (OK)

drop table if exists temp_Link_Papers;
create table temp_Link_Papers (primary key (VALUE_ID))
	select VALUE_ID, ID PAPER_ID
	from (
		select	pts.VALUE_ID, 
				pap.PAPER_ID,
				tnp.ID
		from YAxes yax
		inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
		inner join Datasets ds on yax._dataset_DATASET_ID = ds.DATASET_ID
		inner join Papers pap on ds._paper_PAPER_ID = pap.PAPER_ID
		inner join temp_NewOldId_Papers tnp on pap.PAPER_ID = tnp.PAPER_ID
	) tmp;

-- Y <------> Observable (OK)

-- YAxes
drop table if exists temp_Link_YAxisObservable;
create table temp_Link_YAxisObservable (primary key (VALUE_ID))
	select VALUE_ID, ID as AXIS_OBSERVABLE_ID
	from YAxes yax
	inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
	inner join minehep.ObservableDim obs on obs.Name = yax.Observable;

-- DsObservables
drop table if exists temp_RowPerGroup_DatasetsObservable;
create table temp_RowPerGroup_DatasetsObservable (primary key (VALUE_ID, ID))
	select	@row_number := case when @vid = VALUE_ID then @row_number + 1 else 1 end as ROW_NUM,
			@vid := VALUE_ID as VALUE_ID,
			ID
	from (
		select distinct VALUE_ID, ID
		from YAxes yax
		inner join DsObservables ds on yax._dataset_DATASET_ID = ds.DATASET_ID
		inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
		inner join minehep.ObservableDim obs on obs.Name = ds.DsObservables
        order by VALUE_ID
    ) tmp;

drop table if exists temp_Link_DatasetsObservable;
create table temp_Link_DatasetsObservable (primary key (VALUE_ID))
 	select
		VALUE_ID,
		max(case when ROW_NUM=	1  then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_1,
		max(case when ROW_NUM=	2  then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_2,
		max(case when ROW_NUM=	3  then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_3,
		max(case when ROW_NUM=	4  then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_4,
		max(case when ROW_NUM=	5  then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_5,
		max(case when ROW_NUM=	6  then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_6,
		max(case when ROW_NUM=	7  then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_7,
		max(case when ROW_NUM=	8  then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_8,
		max(case when ROW_NUM=	9  then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_9,
		max(case when ROW_NUM=	10 then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_10,
		max(case when ROW_NUM=	11 then tpd.ID	else null end) as DATASET_OBSERVABLE_ID_11
	from temp_RowPerGroup_DatasetsObservable tpd
    group by VALUE_ID;

-- DsObservables + YAxes
drop table if exists temp_Link_Observable;
create table temp_Link_Observable (primary key (VALUE_ID))
	select 	VALUE_ID,
			max(case when AXIS_OBSERVABLE_ID is not null then AXIS_OBSERVABLE_ID else null end) as AXIS_OBSERVABLE_ID,
			max(case when DATASET_OBSERVABLE_ID_1  is not null then DATASET_OBSERVABLE_ID_1  else null end) as DATASET_OBSERVABLE_ID_1,
			max(case when DATASET_OBSERVABLE_ID_2  is not null then DATASET_OBSERVABLE_ID_2  else null end) as DATASET_OBSERVABLE_ID_2,
			max(case when DATASET_OBSERVABLE_ID_3  is not null then DATASET_OBSERVABLE_ID_3  else null end) as DATASET_OBSERVABLE_ID_3,
			max(case when DATASET_OBSERVABLE_ID_4  is not null then DATASET_OBSERVABLE_ID_4  else null end) as DATASET_OBSERVABLE_ID_4,
			max(case when DATASET_OBSERVABLE_ID_5  is not null then DATASET_OBSERVABLE_ID_5  else null end) as DATASET_OBSERVABLE_ID_5,
			max(case when DATASET_OBSERVABLE_ID_6  is not null then DATASET_OBSERVABLE_ID_6  else null end) as DATASET_OBSERVABLE_ID_6,
			max(case when DATASET_OBSERVABLE_ID_7  is not null then DATASET_OBSERVABLE_ID_7  else null end) as DATASET_OBSERVABLE_ID_7,
			max(case when DATASET_OBSERVABLE_ID_8  is not null then DATASET_OBSERVABLE_ID_8  else null end) as DATASET_OBSERVABLE_ID_8,
			max(case when DATASET_OBSERVABLE_ID_9  is not null then DATASET_OBSERVABLE_ID_9  else null end) as DATASET_OBSERVABLE_ID_9,
			max(case when DATASET_OBSERVABLE_ID_10 is not null then DATASET_OBSERVABLE_ID_10 else null end) as DATASET_OBSERVABLE_ID_10,
			max(case when DATASET_OBSERVABLE_ID_11 is not null then DATASET_OBSERVABLE_ID_11 else null end) as DATASET_OBSERVABLE_ID_11
	from (
		select 	VALUE_ID, 
				null as AXIS_OBSERVABLE_ID,
                DATASET_OBSERVABLE_ID_1,
                DATASET_OBSERVABLE_ID_2,
                DATASET_OBSERVABLE_ID_3,
                DATASET_OBSERVABLE_ID_4,
                DATASET_OBSERVABLE_ID_5,
                DATASET_OBSERVABLE_ID_6,
                DATASET_OBSERVABLE_ID_7,
                DATASET_OBSERVABLE_ID_8,
                DATASET_OBSERVABLE_ID_9,
                DATASET_OBSERVABLE_ID_10,
                DATASET_OBSERVABLE_ID_11
		from temp_Link_DatasetsObservable
		union
		select 	VALUE_ID, 
				AXIS_OBSERVABLE_ID,
                null as DATASET_OBSERVABLE_ID_1,
                null as DATASET_OBSERVABLE_ID_2,
                null as DATASET_OBSERVABLE_ID_3,
                null as DATASET_OBSERVABLE_ID_4,
                null as DATASET_OBSERVABLE_ID_5,
                null as DATASET_OBSERVABLE_ID_6,
                null as DATASET_OBSERVABLE_ID_7,
                null as DATASET_OBSERVABLE_ID_8,
                null as DATASET_OBSERVABLE_ID_9,
                null as DATASET_OBSERVABLE_ID_10,
                null as DATASET_OBSERVABLE_ID_11
		from temp_Link_YAxisObservable
	) tmp
	group by VALUE_ID;
    
-- Y <------> YErrorDim (NOT TESTED)

-- YAxisError
drop table if exists temp_RowPerGroup_AxisError;
create table temp_RowPerGroup_AxisError (primary key (VALUE_ID, ID))
	select	@row_number := case when @vid = VALUE_ID then @row_number + 1 else 1 end as ROW_NUM,
			@vid := VALUE_ID as VALUE_ID,
			ID
	from (
		select distinct VALUE_ID, ID
		from AxisErrors yer
		inner join Points pts on pts._yAxis_AXIS_ID = yer.AXIS_ID
		inner join minehep.YErrorDim erd on erd.Comment <=> yer.Comment and erd.MinusError <=> yer.MinusError and erd.MinusErrorLength <=> yer.MinusErrorLength and erd.Norm <=> yer.Norm and erd.PlusError <=> yer.PlusError and erd.PlusErrorLength <=> yer.PlusErrorLength and erd.Source <=> yer.Source
		order by VALUE_ID
    ) tmp;

drop table if exists temp_Link_AxisError;
create table temp_Link_AxisError (primary key (VALUE_ID))
 	select
		VALUE_ID,
		max(case when ROW_NUM=	1  then axr.ID	else null end) as AXIS_ERROR_ID_1,
		max(case when ROW_NUM=	2  then axr.ID	else null end) as AXIS_ERROR_ID_2,
		max(case when ROW_NUM=	3  then axr.ID	else null end) as AXIS_ERROR_ID_3,
		max(case when ROW_NUM=	4  then axr.ID	else null end) as AXIS_ERROR_ID_4,
		max(case when ROW_NUM=	5  then axr.ID	else null end) as AXIS_ERROR_ID_5,
		max(case when ROW_NUM=	6  then axr.ID	else null end) as AXIS_ERROR_ID_6,
		max(case when ROW_NUM=	7  then axr.ID	else null end) as AXIS_ERROR_ID_7
	from temp_RowPerGroup_AxisError axr
    group by VALUE_ID;

-- DatasetErrors
drop table if exists temp_RowPerGroup_DatasetErrors;
create table temp_RowPerGroup_DatasetErrors (primary key (VALUE_ID, ID))
	select	@row_number := case when @vid = VALUE_ID then @row_number + 1 else 1 end as ROW_NUM,
			@vid := VALUE_ID as VALUE_ID,
			ID
	from (
		select distinct VALUE_ID, ID
		from DatasetErrors dse
		inner join YAxes yax on yax._dataset_DATASET_ID = dse.DATASET_ID
		inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
		inner join minehep.YErrorDim erd on erd.Comment <=> dse.Comment and erd.MinusError <=> dse.MinusError and erd.MinusErrorLength <=> dse.MinusErrorLength and erd.Norm <=> dse.Norm and erd.PlusError <=> dse.PlusError and erd.PlusErrorLength <=> dse.PlusErrorLength and erd.Source <=> dse.Source
        order by VALUE_ID
    ) tmp;

drop table if exists temp_Link_DatasetErrors;
create table temp_Link_DatasetErrors (primary key (VALUE_ID))
 	select
		VALUE_ID,
		max(case when ROW_NUM=1  then dse.ID else null end) as DATASET_ERROR_ID_1,
		max(case when ROW_NUM=2  then dse.ID else null end) as DATASET_ERROR_ID_2,
		max(case when ROW_NUM=3  then dse.ID else null end) as DATASET_ERROR_ID_3,
		max(case when ROW_NUM=4  then dse.ID else null end) as DATASET_ERROR_ID_4,
		max(case when ROW_NUM=5  then dse.ID else null end) as DATASET_ERROR_ID_5,
		max(case when ROW_NUM=6  then dse.ID else null end) as DATASET_ERROR_ID_6,
		max(case when ROW_NUM=7  then dse.ID else null end) as DATASET_ERROR_ID_7,
		max(case when ROW_NUM=8  then dse.ID else null end) as DATASET_ERROR_ID_8,
		max(case when ROW_NUM=9  then dse.ID else null end) as DATASET_ERROR_ID_9,
		max(case when ROW_NUM=10 then dse.ID else null end) as DATASET_ERROR_ID_10,
		max(case when ROW_NUM=11 then dse.ID else null end) as DATASET_ERROR_ID_11,
		max(case when ROW_NUM=12 then dse.ID else null end) as DATASET_ERROR_ID_12,
		max(case when ROW_NUM=13 then dse.ID else null end) as DATASET_ERROR_ID_13,
		max(case when ROW_NUM=14 then dse.ID else null end) as DATASET_ERROR_ID_14,
		max(case when ROW_NUM=15 then dse.ID else null end) as DATASET_ERROR_ID_15,
		max(case when ROW_NUM=16 then dse.ID else null end) as DATASET_ERROR_ID_16,
		max(case when ROW_NUM=17 then dse.ID else null end) as DATASET_ERROR_ID_17,
		max(case when ROW_NUM=18 then dse.ID else null end) as DATASET_ERROR_ID_18,
		max(case when ROW_NUM=19 then dse.ID else null end) as DATASET_ERROR_ID_19
	from temp_RowPerGroup_DatasetErrors dse
    group by VALUE_ID;

-- PointErrors
drop table if exists temp_RowPerGroup_PointErrors;
create table temp_RowPerGroup_PointErrors (primary key (VALUE_ID, ID))
	select	@row_number := case when @vid = VALUE_ID then @row_number + 1 else 1 end as ROW_NUM,
			@vid := VALUE_ID as VALUE_ID,
			ID
	from (
		select distinct VALUE_ID, ID
		from PointErrors pte
		inner join minehep.YErrorDim erd on erd.Comment <=> pte.Comment and erd.MinusError <=> pte.MinusError and erd.MinusErrorLength <=> pte.MinusErrorLength and erd.Norm <=> pte.Norm and erd.PlusError <=> pte.PlusError and erd.PlusErrorLength <=> pte.PlusErrorLength and erd.Source <=> pte.Source
        order by VALUE_ID
    ) tmp;

drop table if exists temp_Link_PointErrors;
create table temp_Link_PointErrors (primary key (VALUE_ID))
 	select
		VALUE_ID,
		max(case when ROW_NUM=1  then pte.ID else null end) as POINT_ERROR_ID_1,
		max(case when ROW_NUM=2  then pte.ID else null end) as POINT_ERROR_ID_2,
		max(case when ROW_NUM=3  then pte.ID else null end) as POINT_ERROR_ID_3,
		max(case when ROW_NUM=4  then pte.ID else null end) as POINT_ERROR_ID_4,
		max(case when ROW_NUM=5  then pte.ID else null end) as POINT_ERROR_ID_5,
		max(case when ROW_NUM=6  then pte.ID else null end) as POINT_ERROR_ID_6,
		max(case when ROW_NUM=7  then pte.ID else null end) as POINT_ERROR_ID_7,
		max(case when ROW_NUM=8  then pte.ID else null end) as POINT_ERROR_ID_8,
		max(case when ROW_NUM=9  then pte.ID else null end) as POINT_ERROR_ID_9,
		max(case when ROW_NUM=10 then pte.ID else null end) as POINT_ERROR_ID_10,
		max(case when ROW_NUM=11 then pte.ID else null end) as POINT_ERROR_ID_11,
		max(case when ROW_NUM=12 then pte.ID else null end) as POINT_ERROR_ID_12,
		max(case when ROW_NUM=13 then pte.ID else null end) as POINT_ERROR_ID_13,
		max(case when ROW_NUM=14 then pte.ID else null end) as POINT_ERROR_ID_14,
		max(case when ROW_NUM=15 then pte.ID else null end) as POINT_ERROR_ID_15,
		max(case when ROW_NUM=16 then pte.ID else null end) as POINT_ERROR_ID_16,
		max(case when ROW_NUM=17 then pte.ID else null end) as POINT_ERROR_ID_17,
		max(case when ROW_NUM=18 then pte.ID else null end) as POINT_ERROR_ID_18,
		max(case when ROW_NUM=19 then pte.ID else null end) as POINT_ERROR_ID_19,
		max(case when ROW_NUM=20 then pte.ID else null end) as POINT_ERROR_ID_20,
		max(case when ROW_NUM=21 then pte.ID else null end) as POINT_ERROR_ID_21,
		max(case when ROW_NUM=22 then pte.ID else null end) as POINT_ERROR_ID_22,
		max(case when ROW_NUM=23 then pte.ID else null end) as POINT_ERROR_ID_23,
		max(case when ROW_NUM=24 then pte.ID else null end) as POINT_ERROR_ID_24,
		max(case when ROW_NUM=25 then pte.ID else null end) as POINT_ERROR_ID_25,
		max(case when ROW_NUM=26 then pte.ID else null end) as POINT_ERROR_ID_26,
		max(case when ROW_NUM=27 then pte.ID else null end) as POINT_ERROR_ID_27,
		max(case when ROW_NUM=28 then pte.ID else null end) as POINT_ERROR_ID_28,
		max(case when ROW_NUM=29 then pte.ID else null end) as POINT_ERROR_ID_29,
		max(case when ROW_NUM=30 then pte.ID else null end) as POINT_ERROR_ID_30,
		max(case when ROW_NUM=31 then pte.ID else null end) as POINT_ERROR_ID_31,
		max(case when ROW_NUM=32 then pte.ID else null end) as POINT_ERROR_ID_32,
		max(case when ROW_NUM=33 then pte.ID else null end) as POINT_ERROR_ID_33,
		max(case when ROW_NUM=34 then pte.ID else null end) as POINT_ERROR_ID_34,
		max(case when ROW_NUM=35 then pte.ID else null end) as POINT_ERROR_ID_35,
		max(case when ROW_NUM=36 then pte.ID else null end) as POINT_ERROR_ID_36,
		max(case when ROW_NUM=37 then pte.ID else null end) as POINT_ERROR_ID_37,
		max(case when ROW_NUM=38 then pte.ID else null end) as POINT_ERROR_ID_38,
		max(case when ROW_NUM=39 then pte.ID else null end) as POINT_ERROR_ID_39,
		max(case when ROW_NUM=40 then pte.ID else null end) as POINT_ERROR_ID_40,
		max(case when ROW_NUM=41 then pte.ID else null end) as POINT_ERROR_ID_41,
		max(case when ROW_NUM=42 then pte.ID else null end) as POINT_ERROR_ID_42,
		max(case when ROW_NUM=43 then pte.ID else null end) as POINT_ERROR_ID_43,
		max(case when ROW_NUM=44 then pte.ID else null end) as POINT_ERROR_ID_44,
		max(case when ROW_NUM=45 then pte.ID else null end) as POINT_ERROR_ID_45,
		max(case when ROW_NUM=46 then pte.ID else null end) as POINT_ERROR_ID_46,
		max(case when ROW_NUM=47 then pte.ID else null end) as POINT_ERROR_ID_47,
		max(case when ROW_NUM=48 then pte.ID else null end) as POINT_ERROR_ID_48,
		max(case when ROW_NUM=49 then pte.ID else null end) as POINT_ERROR_ID_49,
		max(case when ROW_NUM=50 then pte.ID else null end) as POINT_ERROR_ID_50,
		max(case when ROW_NUM=51 then pte.ID else null end) as POINT_ERROR_ID_51,
		max(case when ROW_NUM=52 then pte.ID else null end) as POINT_ERROR_ID_52,
		max(case when ROW_NUM=53 then pte.ID else null end) as POINT_ERROR_ID_53,
		max(case when ROW_NUM=54 then pte.ID else null end) as POINT_ERROR_ID_54,
		max(case when ROW_NUM=55 then pte.ID else null end) as POINT_ERROR_ID_55,
		max(case when ROW_NUM=56 then pte.ID else null end) as POINT_ERROR_ID_56,
		max(case when ROW_NUM=57 then pte.ID else null end) as POINT_ERROR_ID_57,
		max(case when ROW_NUM=58 then pte.ID else null end) as POINT_ERROR_ID_58,
		max(case when ROW_NUM=59 then pte.ID else null end) as POINT_ERROR_ID_59,
		max(case when ROW_NUM=60 then pte.ID else null end) as POINT_ERROR_ID_60,
		max(case when ROW_NUM=61 then pte.ID else null end) as POINT_ERROR_ID_61,
		max(case when ROW_NUM=62 then pte.ID else null end) as POINT_ERROR_ID_62,
		max(case when ROW_NUM=63 then pte.ID else null end) as POINT_ERROR_ID_63,
		max(case when ROW_NUM=64 then pte.ID else null end) as POINT_ERROR_ID_64,
		max(case when ROW_NUM=65 then pte.ID else null end) as POINT_ERROR_ID_65,
		max(case when ROW_NUM=66 then pte.ID else null end) as POINT_ERROR_ID_66,
		max(case when ROW_NUM=67 then pte.ID else null end) as POINT_ERROR_ID_67,
		max(case when ROW_NUM=68 then pte.ID else null end) as POINT_ERROR_ID_68,
		max(case when ROW_NUM=69 then pte.ID else null end) as POINT_ERROR_ID_69,
		max(case when ROW_NUM=70 then pte.ID else null end) as POINT_ERROR_ID_70,
		max(case when ROW_NUM=71 then pte.ID else null end) as POINT_ERROR_ID_71
	from temp_RowPerGroup_PointErrors pte
    group by VALUE_ID;

-- YAxisError + DatasetErrors + PointErrors
drop table if exists temp_Link_Errors;
create table temp_Link_Errors (primary key (VALUE_ID))
	select 	VALUE_ID,
			max(case when POINT_ERROR_ID_1	is	not	null then POINT_ERROR_ID_1	else null end) as POINT_ERROR_ID_1,
			max(case when POINT_ERROR_ID_2	is	not	null then POINT_ERROR_ID_2	else null end) as POINT_ERROR_ID_2,
			max(case when POINT_ERROR_ID_3	is	not	null then POINT_ERROR_ID_3	else null end) as POINT_ERROR_ID_3,
			max(case when POINT_ERROR_ID_4	is	not	null then POINT_ERROR_ID_4	else null end) as POINT_ERROR_ID_4,
			max(case when POINT_ERROR_ID_5	is	not	null then POINT_ERROR_ID_5	else null end) as POINT_ERROR_ID_5,
			max(case when POINT_ERROR_ID_6	is	not	null then POINT_ERROR_ID_6	else null end) as POINT_ERROR_ID_6,
			max(case when POINT_ERROR_ID_7	is	not	null then POINT_ERROR_ID_7	else null end) as POINT_ERROR_ID_7,
			max(case when POINT_ERROR_ID_8	is	not	null then POINT_ERROR_ID_8	else null end) as POINT_ERROR_ID_8,
			max(case when POINT_ERROR_ID_9	is	not	null then POINT_ERROR_ID_9	else null end) as POINT_ERROR_ID_9,
			max(case when POINT_ERROR_ID_10	is	not	null then POINT_ERROR_ID_10	else null end) as POINT_ERROR_ID_10,
			max(case when POINT_ERROR_ID_11	is	not	null then POINT_ERROR_ID_11	else null end) as POINT_ERROR_ID_11,
			max(case when POINT_ERROR_ID_12	is	not	null then POINT_ERROR_ID_12	else null end) as POINT_ERROR_ID_12,
			max(case when POINT_ERROR_ID_13	is	not	null then POINT_ERROR_ID_13	else null end) as POINT_ERROR_ID_13,
			max(case when POINT_ERROR_ID_14	is	not	null then POINT_ERROR_ID_14	else null end) as POINT_ERROR_ID_14,
			max(case when POINT_ERROR_ID_15	is	not	null then POINT_ERROR_ID_15	else null end) as POINT_ERROR_ID_15,
			max(case when POINT_ERROR_ID_16	is	not	null then POINT_ERROR_ID_16	else null end) as POINT_ERROR_ID_16,
			max(case when POINT_ERROR_ID_17	is	not	null then POINT_ERROR_ID_17	else null end) as POINT_ERROR_ID_17,
			max(case when POINT_ERROR_ID_18	is	not	null then POINT_ERROR_ID_18	else null end) as POINT_ERROR_ID_18,
			max(case when POINT_ERROR_ID_19	is	not	null then POINT_ERROR_ID_19	else null end) as POINT_ERROR_ID_19,
			max(case when POINT_ERROR_ID_20	is	not	null then POINT_ERROR_ID_20	else null end) as POINT_ERROR_ID_20,
			max(case when POINT_ERROR_ID_21	is	not	null then POINT_ERROR_ID_21	else null end) as POINT_ERROR_ID_21,
			max(case when POINT_ERROR_ID_22	is	not	null then POINT_ERROR_ID_22	else null end) as POINT_ERROR_ID_22,
			max(case when POINT_ERROR_ID_23	is	not	null then POINT_ERROR_ID_23	else null end) as POINT_ERROR_ID_23,
			max(case when POINT_ERROR_ID_24	is	not	null then POINT_ERROR_ID_24	else null end) as POINT_ERROR_ID_24,
			max(case when POINT_ERROR_ID_25	is	not	null then POINT_ERROR_ID_25	else null end) as POINT_ERROR_ID_25,
			max(case when POINT_ERROR_ID_26	is	not	null then POINT_ERROR_ID_26	else null end) as POINT_ERROR_ID_26,
			max(case when POINT_ERROR_ID_27	is	not	null then POINT_ERROR_ID_27	else null end) as POINT_ERROR_ID_27,
			max(case when POINT_ERROR_ID_28	is	not	null then POINT_ERROR_ID_28	else null end) as POINT_ERROR_ID_28,
			max(case when POINT_ERROR_ID_29	is	not	null then POINT_ERROR_ID_29	else null end) as POINT_ERROR_ID_29,
			max(case when POINT_ERROR_ID_30	is	not	null then POINT_ERROR_ID_30	else null end) as POINT_ERROR_ID_30,
			max(case when POINT_ERROR_ID_31	is	not	null then POINT_ERROR_ID_31	else null end) as POINT_ERROR_ID_31,
			max(case when POINT_ERROR_ID_32	is	not	null then POINT_ERROR_ID_32	else null end) as POINT_ERROR_ID_32,
			max(case when POINT_ERROR_ID_33	is	not	null then POINT_ERROR_ID_33	else null end) as POINT_ERROR_ID_33,
			max(case when POINT_ERROR_ID_34	is	not	null then POINT_ERROR_ID_34	else null end) as POINT_ERROR_ID_34,
			max(case when POINT_ERROR_ID_35	is	not	null then POINT_ERROR_ID_35	else null end) as POINT_ERROR_ID_35,
			max(case when POINT_ERROR_ID_36	is	not	null then POINT_ERROR_ID_36	else null end) as POINT_ERROR_ID_36,
			max(case when POINT_ERROR_ID_37	is	not	null then POINT_ERROR_ID_37	else null end) as POINT_ERROR_ID_37,
			max(case when POINT_ERROR_ID_38	is	not	null then POINT_ERROR_ID_38	else null end) as POINT_ERROR_ID_38,
			max(case when POINT_ERROR_ID_39	is	not	null then POINT_ERROR_ID_39	else null end) as POINT_ERROR_ID_39,
			max(case when POINT_ERROR_ID_40	is	not	null then POINT_ERROR_ID_40	else null end) as POINT_ERROR_ID_40,
			max(case when POINT_ERROR_ID_41	is	not	null then POINT_ERROR_ID_41	else null end) as POINT_ERROR_ID_41,
			max(case when POINT_ERROR_ID_42	is	not	null then POINT_ERROR_ID_42	else null end) as POINT_ERROR_ID_42,
			max(case when POINT_ERROR_ID_43	is	not	null then POINT_ERROR_ID_43	else null end) as POINT_ERROR_ID_43,
			max(case when POINT_ERROR_ID_44	is	not	null then POINT_ERROR_ID_44	else null end) as POINT_ERROR_ID_44,
			max(case when POINT_ERROR_ID_45	is	not	null then POINT_ERROR_ID_45	else null end) as POINT_ERROR_ID_45,
			max(case when POINT_ERROR_ID_46	is	not	null then POINT_ERROR_ID_46	else null end) as POINT_ERROR_ID_46,
			max(case when POINT_ERROR_ID_47	is	not	null then POINT_ERROR_ID_47	else null end) as POINT_ERROR_ID_47,
			max(case when POINT_ERROR_ID_48	is	not	null then POINT_ERROR_ID_48	else null end) as POINT_ERROR_ID_48,
			max(case when POINT_ERROR_ID_49	is	not	null then POINT_ERROR_ID_49	else null end) as POINT_ERROR_ID_49,
			max(case when POINT_ERROR_ID_50	is	not	null then POINT_ERROR_ID_50	else null end) as POINT_ERROR_ID_50,
			max(case when POINT_ERROR_ID_51	is	not	null then POINT_ERROR_ID_51	else null end) as POINT_ERROR_ID_51,
			max(case when POINT_ERROR_ID_52	is	not	null then POINT_ERROR_ID_52	else null end) as POINT_ERROR_ID_52,
			max(case when POINT_ERROR_ID_53	is	not	null then POINT_ERROR_ID_53	else null end) as POINT_ERROR_ID_53,
			max(case when POINT_ERROR_ID_54	is	not	null then POINT_ERROR_ID_54	else null end) as POINT_ERROR_ID_54,
			max(case when POINT_ERROR_ID_55	is	not	null then POINT_ERROR_ID_55	else null end) as POINT_ERROR_ID_55,
			max(case when POINT_ERROR_ID_56	is	not	null then POINT_ERROR_ID_56	else null end) as POINT_ERROR_ID_56,
			max(case when POINT_ERROR_ID_57	is	not	null then POINT_ERROR_ID_57	else null end) as POINT_ERROR_ID_57,
			max(case when POINT_ERROR_ID_58	is	not	null then POINT_ERROR_ID_58	else null end) as POINT_ERROR_ID_58,
			max(case when POINT_ERROR_ID_59	is	not	null then POINT_ERROR_ID_59	else null end) as POINT_ERROR_ID_59,
			max(case when POINT_ERROR_ID_60	is	not	null then POINT_ERROR_ID_60	else null end) as POINT_ERROR_ID_60,
			max(case when POINT_ERROR_ID_61	is	not	null then POINT_ERROR_ID_61	else null end) as POINT_ERROR_ID_61,
			max(case when POINT_ERROR_ID_62	is	not	null then POINT_ERROR_ID_62	else null end) as POINT_ERROR_ID_62,
			max(case when POINT_ERROR_ID_63	is	not	null then POINT_ERROR_ID_63	else null end) as POINT_ERROR_ID_63,
			max(case when POINT_ERROR_ID_64	is	not	null then POINT_ERROR_ID_64	else null end) as POINT_ERROR_ID_64,
			max(case when POINT_ERROR_ID_65	is	not	null then POINT_ERROR_ID_65	else null end) as POINT_ERROR_ID_65,
			max(case when POINT_ERROR_ID_66	is	not	null then POINT_ERROR_ID_66	else null end) as POINT_ERROR_ID_66,
			max(case when POINT_ERROR_ID_67	is	not	null then POINT_ERROR_ID_67	else null end) as POINT_ERROR_ID_67,
			max(case when POINT_ERROR_ID_68	is	not	null then POINT_ERROR_ID_68	else null end) as POINT_ERROR_ID_68,
			max(case when POINT_ERROR_ID_69	is	not	null then POINT_ERROR_ID_69	else null end) as POINT_ERROR_ID_69,
			max(case when POINT_ERROR_ID_70	is	not	null then POINT_ERROR_ID_70	else null end) as POINT_ERROR_ID_70,
			max(case when POINT_ERROR_ID_71	is	not	null then POINT_ERROR_ID_71	else null end) as POINT_ERROR_ID_71,
			max(case when AXIS_ERROR_ID_1	is	not	null then AXIS_ERROR_ID_1 else null	end) as	AXIS_ERROR_ID_1,
			max(case when AXIS_ERROR_ID_2	is	not	null then AXIS_ERROR_ID_2 else null	end) as	AXIS_ERROR_ID_2,
			max(case when AXIS_ERROR_ID_3	is	not	null then AXIS_ERROR_ID_3 else null	end) as	AXIS_ERROR_ID_3,
			max(case when AXIS_ERROR_ID_4	is	not	null then AXIS_ERROR_ID_4 else null	end) as	AXIS_ERROR_ID_4,
			max(case when AXIS_ERROR_ID_5	is	not	null then AXIS_ERROR_ID_5 else null	end) as	AXIS_ERROR_ID_5,
			max(case when AXIS_ERROR_ID_6	is	not	null then AXIS_ERROR_ID_6 else null	end) as	AXIS_ERROR_ID_6,
			max(case when AXIS_ERROR_ID_7	is	not	null then AXIS_ERROR_ID_7 else null	end) as	AXIS_ERROR_ID_7,
			max(case when DATASET_ERROR_ID_1 is not	null then DATASET_ERROR_ID_1 else null end) as DATASET_ERROR_ID_1,
			max(case when DATASET_ERROR_ID_2 is not	null then DATASET_ERROR_ID_2 else null end) as DATASET_ERROR_ID_2,
			max(case when DATASET_ERROR_ID_3 is not	null then DATASET_ERROR_ID_3 else null end) as DATASET_ERROR_ID_3,
			max(case when DATASET_ERROR_ID_4 is not	null then DATASET_ERROR_ID_4 else null end) as DATASET_ERROR_ID_4,
			max(case when DATASET_ERROR_ID_5 is not	null then DATASET_ERROR_ID_5 else null end) as DATASET_ERROR_ID_5,
			max(case when DATASET_ERROR_ID_6 is not	null then DATASET_ERROR_ID_6 else null end) as DATASET_ERROR_ID_6,
			max(case when DATASET_ERROR_ID_7 is not	null then DATASET_ERROR_ID_7 else null end) as DATASET_ERROR_ID_7,
			max(case when DATASET_ERROR_ID_8 is not	null then DATASET_ERROR_ID_8 else null end) as DATASET_ERROR_ID_8,
			max(case when DATASET_ERROR_ID_9 is not	null then DATASET_ERROR_ID_9 else null end) as DATASET_ERROR_ID_9,
			max(case when DATASET_ERROR_ID_10 is not null then DATASET_ERROR_ID_10 else null end) as DATASET_ERROR_ID_10,
			max(case when DATASET_ERROR_ID_11 is not null then DATASET_ERROR_ID_11 else null end) as DATASET_ERROR_ID_11,
			max(case when DATASET_ERROR_ID_12 is not null then DATASET_ERROR_ID_12 else null end) as DATASET_ERROR_ID_12,
			max(case when DATASET_ERROR_ID_13 is not null then DATASET_ERROR_ID_13 else null end) as DATASET_ERROR_ID_13,
			max(case when DATASET_ERROR_ID_14 is not null then DATASET_ERROR_ID_14 else null end) as DATASET_ERROR_ID_14,
			max(case when DATASET_ERROR_ID_15 is not null then DATASET_ERROR_ID_15 else null end) as DATASET_ERROR_ID_15,
			max(case when DATASET_ERROR_ID_16 is not null then DATASET_ERROR_ID_16 else null end) as DATASET_ERROR_ID_16,
			max(case when DATASET_ERROR_ID_17 is not null then DATASET_ERROR_ID_17 else null end) as DATASET_ERROR_ID_17,
			max(case when DATASET_ERROR_ID_18 is not null then DATASET_ERROR_ID_18 else null end) as DATASET_ERROR_ID_18,
			max(case when DATASET_ERROR_ID_19 is not null then DATASET_ERROR_ID_19 else null end) as DATASET_ERROR_ID_19
	from (
		select 	VALUE_ID, 
				POINT_ERROR_ID_1,
				POINT_ERROR_ID_2,
				POINT_ERROR_ID_3,
				POINT_ERROR_ID_4,
				POINT_ERROR_ID_5,
				POINT_ERROR_ID_6,
				POINT_ERROR_ID_7,
				POINT_ERROR_ID_8,
				POINT_ERROR_ID_9,
				POINT_ERROR_ID_10,
				POINT_ERROR_ID_11,
				POINT_ERROR_ID_12,
				POINT_ERROR_ID_13,
				POINT_ERROR_ID_14,
				POINT_ERROR_ID_15,
				POINT_ERROR_ID_16,
				POINT_ERROR_ID_17,
				POINT_ERROR_ID_18,
				POINT_ERROR_ID_19,
				POINT_ERROR_ID_20,
				POINT_ERROR_ID_21,
				POINT_ERROR_ID_22,
				POINT_ERROR_ID_23,
				POINT_ERROR_ID_24,
				POINT_ERROR_ID_25,
				POINT_ERROR_ID_26,
				POINT_ERROR_ID_27,
				POINT_ERROR_ID_28,
				POINT_ERROR_ID_29,
				POINT_ERROR_ID_30,
				POINT_ERROR_ID_31,
				POINT_ERROR_ID_32,
				POINT_ERROR_ID_33,
				POINT_ERROR_ID_34,
				POINT_ERROR_ID_35,
				POINT_ERROR_ID_36,
				POINT_ERROR_ID_37,
				POINT_ERROR_ID_38,
				POINT_ERROR_ID_39,
				POINT_ERROR_ID_40,
				POINT_ERROR_ID_41,
				POINT_ERROR_ID_42,
				POINT_ERROR_ID_43,
				POINT_ERROR_ID_44,
				POINT_ERROR_ID_45,
				POINT_ERROR_ID_46,
				POINT_ERROR_ID_47,
				POINT_ERROR_ID_48,
				POINT_ERROR_ID_49,
				POINT_ERROR_ID_50,
				POINT_ERROR_ID_51,
				POINT_ERROR_ID_52,
				POINT_ERROR_ID_53,
				POINT_ERROR_ID_54,
				POINT_ERROR_ID_55,
				POINT_ERROR_ID_56,
				POINT_ERROR_ID_57,
				POINT_ERROR_ID_58,
				POINT_ERROR_ID_59,
				POINT_ERROR_ID_60,
				POINT_ERROR_ID_61,
				POINT_ERROR_ID_62,
				POINT_ERROR_ID_63,
				POINT_ERROR_ID_64,
				POINT_ERROR_ID_65,
				POINT_ERROR_ID_66,
				POINT_ERROR_ID_67,
				POINT_ERROR_ID_68,
				POINT_ERROR_ID_69,
				POINT_ERROR_ID_70,
				POINT_ERROR_ID_71,
				null as	AXIS_ERROR_ID_1,
				null as	AXIS_ERROR_ID_2,
				null as	AXIS_ERROR_ID_3,
				null as	AXIS_ERROR_ID_4,
				null as	AXIS_ERROR_ID_5,
				null as	AXIS_ERROR_ID_6,
				null as	AXIS_ERROR_ID_7,
				null as	DATASET_ERROR_ID_1,
				null as	DATASET_ERROR_ID_2,
				null as	DATASET_ERROR_ID_3,
				null as	DATASET_ERROR_ID_4,
				null as	DATASET_ERROR_ID_5,
				null as	DATASET_ERROR_ID_6,
				null as	DATASET_ERROR_ID_7,
				null as	DATASET_ERROR_ID_8,
				null as	DATASET_ERROR_ID_9,
				null as	DATASET_ERROR_ID_10,
				null as	DATASET_ERROR_ID_11,
				null as	DATASET_ERROR_ID_12,
				null as	DATASET_ERROR_ID_13,
				null as	DATASET_ERROR_ID_14,
				null as	DATASET_ERROR_ID_15,
				null as	DATASET_ERROR_ID_16,
				null as	DATASET_ERROR_ID_17,
				null as	DATASET_ERROR_ID_18,
				null as	DATASET_ERROR_ID_19	
		from temp_Link_PointErrors
		union
		select 	VALUE_ID, 
				null as	POINT_ERROR_ID_1,
				null as	POINT_ERROR_ID_2,
				null as	POINT_ERROR_ID_3,
				null as	POINT_ERROR_ID_4,
				null as	POINT_ERROR_ID_5,
				null as	POINT_ERROR_ID_6,
				null as	POINT_ERROR_ID_7,
				null as	POINT_ERROR_ID_8,
				null as	POINT_ERROR_ID_9,
				null as	POINT_ERROR_ID_10,
				null as	POINT_ERROR_ID_11,
				null as	POINT_ERROR_ID_12,
				null as	POINT_ERROR_ID_13,
				null as	POINT_ERROR_ID_14,
				null as	POINT_ERROR_ID_15,
				null as	POINT_ERROR_ID_16,
				null as	POINT_ERROR_ID_17,
				null as	POINT_ERROR_ID_18,
				null as	POINT_ERROR_ID_19,
				null as	POINT_ERROR_ID_20,
				null as	POINT_ERROR_ID_21,
				null as	POINT_ERROR_ID_22,
				null as	POINT_ERROR_ID_23,
				null as	POINT_ERROR_ID_24,
				null as	POINT_ERROR_ID_25,
				null as	POINT_ERROR_ID_26,
				null as	POINT_ERROR_ID_27,
				null as	POINT_ERROR_ID_28,
				null as	POINT_ERROR_ID_29,
				null as	POINT_ERROR_ID_30,
				null as	POINT_ERROR_ID_31,
				null as	POINT_ERROR_ID_32,
				null as	POINT_ERROR_ID_33,
				null as	POINT_ERROR_ID_34,
				null as	POINT_ERROR_ID_35,
				null as	POINT_ERROR_ID_36,
				null as	POINT_ERROR_ID_37,
				null as	POINT_ERROR_ID_38,
				null as	POINT_ERROR_ID_39,
				null as	POINT_ERROR_ID_40,
				null as	POINT_ERROR_ID_41,
				null as	POINT_ERROR_ID_42,
				null as	POINT_ERROR_ID_43,
				null as	POINT_ERROR_ID_44,
				null as	POINT_ERROR_ID_45,
				null as	POINT_ERROR_ID_46,
				null as	POINT_ERROR_ID_47,
				null as	POINT_ERROR_ID_48,
				null as	POINT_ERROR_ID_49,
				null as	POINT_ERROR_ID_50,
				null as	POINT_ERROR_ID_51,
				null as	POINT_ERROR_ID_52,
				null as	POINT_ERROR_ID_53,
				null as	POINT_ERROR_ID_54,
				null as	POINT_ERROR_ID_55,
				null as	POINT_ERROR_ID_56,
				null as	POINT_ERROR_ID_57,
				null as	POINT_ERROR_ID_58,
				null as	POINT_ERROR_ID_59,
				null as	POINT_ERROR_ID_60,
				null as	POINT_ERROR_ID_61,
				null as	POINT_ERROR_ID_62,
				null as	POINT_ERROR_ID_63,
				null as	POINT_ERROR_ID_64,
				null as	POINT_ERROR_ID_65,
				null as	POINT_ERROR_ID_66,
				null as	POINT_ERROR_ID_67,
				null as	POINT_ERROR_ID_68,
				null as	POINT_ERROR_ID_69,
				null as	POINT_ERROR_ID_70,
				null as	POINT_ERROR_ID_71,
				AXIS_ERROR_ID_1,
				AXIS_ERROR_ID_2,
				AXIS_ERROR_ID_3,
				AXIS_ERROR_ID_4,
				AXIS_ERROR_ID_5,
				AXIS_ERROR_ID_6,
				AXIS_ERROR_ID_7,
				null as	DATASET_ERROR_ID_1,
				null as	DATASET_ERROR_ID_2,
				null as	DATASET_ERROR_ID_3,
				null as	DATASET_ERROR_ID_4,
				null as	DATASET_ERROR_ID_5,
				null as	DATASET_ERROR_ID_6,
				null as	DATASET_ERROR_ID_7,
				null as	DATASET_ERROR_ID_8,
				null as	DATASET_ERROR_ID_9,
				null as	DATASET_ERROR_ID_10,
				null as	DATASET_ERROR_ID_11,
				null as	DATASET_ERROR_ID_12,
				null as	DATASET_ERROR_ID_13,
				null as	DATASET_ERROR_ID_14,
				null as	DATASET_ERROR_ID_15,
				null as	DATASET_ERROR_ID_16,
				null as	DATASET_ERROR_ID_17,
				null as	DATASET_ERROR_ID_18,
				null as	DATASET_ERROR_ID_19	
		from temp_Link_AxisError
		union
		select 	VALUE_ID, 
				null as	POINT_ERROR_ID_1,
				null as	POINT_ERROR_ID_2,
				null as	POINT_ERROR_ID_3,
				null as	POINT_ERROR_ID_4,
				null as	POINT_ERROR_ID_5,
				null as	POINT_ERROR_ID_6,
				null as	POINT_ERROR_ID_7,
				null as	POINT_ERROR_ID_8,
				null as	POINT_ERROR_ID_9,
				null as	POINT_ERROR_ID_10,
				null as	POINT_ERROR_ID_11,
				null as	POINT_ERROR_ID_12,
				null as	POINT_ERROR_ID_13,
				null as	POINT_ERROR_ID_14,
				null as	POINT_ERROR_ID_15,
				null as	POINT_ERROR_ID_16,
				null as	POINT_ERROR_ID_17,
				null as	POINT_ERROR_ID_18,
				null as	POINT_ERROR_ID_19,
				null as	POINT_ERROR_ID_20,
				null as	POINT_ERROR_ID_21,
				null as	POINT_ERROR_ID_22,
				null as	POINT_ERROR_ID_23,
				null as	POINT_ERROR_ID_24,
				null as	POINT_ERROR_ID_25,
				null as	POINT_ERROR_ID_26,
				null as	POINT_ERROR_ID_27,
				null as	POINT_ERROR_ID_28,
				null as	POINT_ERROR_ID_29,
				null as	POINT_ERROR_ID_30,
				null as	POINT_ERROR_ID_31,
				null as	POINT_ERROR_ID_32,
				null as	POINT_ERROR_ID_33,
				null as	POINT_ERROR_ID_34,
				null as	POINT_ERROR_ID_35,
				null as	POINT_ERROR_ID_36,
				null as	POINT_ERROR_ID_37,
				null as	POINT_ERROR_ID_38,
				null as	POINT_ERROR_ID_39,
				null as	POINT_ERROR_ID_40,
				null as	POINT_ERROR_ID_41,
				null as	POINT_ERROR_ID_42,
				null as	POINT_ERROR_ID_43,
				null as	POINT_ERROR_ID_44,
				null as	POINT_ERROR_ID_45,
				null as	POINT_ERROR_ID_46,
				null as	POINT_ERROR_ID_47,
				null as	POINT_ERROR_ID_48,
				null as	POINT_ERROR_ID_49,
				null as	POINT_ERROR_ID_50,
				null as	POINT_ERROR_ID_51,
				null as	POINT_ERROR_ID_52,
				null as	POINT_ERROR_ID_53,
				null as	POINT_ERROR_ID_54,
				null as	POINT_ERROR_ID_55,
				null as	POINT_ERROR_ID_56,
				null as	POINT_ERROR_ID_57,
				null as	POINT_ERROR_ID_58,
				null as	POINT_ERROR_ID_59,
				null as	POINT_ERROR_ID_60,
				null as	POINT_ERROR_ID_61,
				null as	POINT_ERROR_ID_62,
				null as	POINT_ERROR_ID_63,
				null as	POINT_ERROR_ID_64,
				null as	POINT_ERROR_ID_65,
				null as	POINT_ERROR_ID_66,
				null as	POINT_ERROR_ID_67,
				null as	POINT_ERROR_ID_68,
				null as	POINT_ERROR_ID_69,
				null as	POINT_ERROR_ID_70,
				null as	POINT_ERROR_ID_71,
				null as	AXIS_ERROR_ID_1,
				null as	AXIS_ERROR_ID_2,
				null as	AXIS_ERROR_ID_3,
				null as	AXIS_ERROR_ID_4,
				null as	AXIS_ERROR_ID_5,
				null as	AXIS_ERROR_ID_6,
				null as	AXIS_ERROR_ID_7,
				DATASET_ERROR_ID_1,
				DATASET_ERROR_ID_2,
				DATASET_ERROR_ID_3,
				DATASET_ERROR_ID_4,
				DATASET_ERROR_ID_5,
				DATASET_ERROR_ID_6,
				DATASET_ERROR_ID_7,
				DATASET_ERROR_ID_8,
				DATASET_ERROR_ID_9,
				DATASET_ERROR_ID_10,
				DATASET_ERROR_ID_11,
				DATASET_ERROR_ID_12,
				DATASET_ERROR_ID_13,
				DATASET_ERROR_ID_14,
				DATASET_ERROR_ID_15,
				DATASET_ERROR_ID_16,
				DATASET_ERROR_ID_17,
				DATASET_ERROR_ID_18,
				DATASET_ERROR_ID_19
		from temp_Link_DatasetErrors
	) tmp
	group by VALUE_ID;
    
-- DsKeywords <------> Y
drop table if exists temp_RowPerGroup_Keywords;
create table temp_RowPerGroup_Keywords (primary key (VALUE_ID, ID))
	select	@row_number := case when @vid = VALUE_ID then @row_number + 1 else 1 end as ROW_NUM,
			@vid := VALUE_ID as VALUE_ID,
			ID
	from (
		select distinct VALUE_ID, ID
		from DsKeywords dsk
		inner join YAxes yax on yax._dataset_DATASET_ID = dsk.DATASET_ID
		inner join Points pts on pts._yAxis_AXIS_ID = yax.AXIS_ID
		inner join minehep.KeywordsDim dik on dik.Name <=> dsk.Keyword
        order by VALUE_ID
    ) tmp;

drop table if exists temp_Link_Keywords;
create table temp_Link_Keywords (primary key (VALUE_ID))
 	select
		VALUE_ID,
		max(case when ROW_NUM=1  then dsk.ID else null end) as KEYWORD_ID_1,
		max(case when ROW_NUM=2  then dsk.ID else null end) as KEYWORD_ID_2,
		max(case when ROW_NUM=3  then dsk.ID else null end) as KEYWORD_ID_3,
		max(case when ROW_NUM=4  then dsk.ID else null end) as KEYWORD_ID_4,
		max(case when ROW_NUM=5  then dsk.ID else null end) as KEYWORD_ID_5,
		max(case when ROW_NUM=6  then dsk.ID else null end) as KEYWORD_ID_6,
		max(case when ROW_NUM=7  then dsk.ID else null end) as KEYWORD_ID_7,
		max(case when ROW_NUM=8  then dsk.ID else null end) as KEYWORD_ID_8,
		max(case when ROW_NUM=9  then dsk.ID else null end) as KEYWORD_ID_9,
		max(case when ROW_NUM=10 then dsk.ID else null end) as KEYWORD_ID_10,
		max(case when ROW_NUM=11 then dsk.ID else null end) as KEYWORD_ID_11,
		max(case when ROW_NUM=12 then dsk.ID else null end) as KEYWORD_ID_12,
		max(case when ROW_NUM=13 then dsk.ID else null end) as KEYWORD_ID_13,
		max(case when ROW_NUM=14 then dsk.ID else null end) as KEYWORD_ID_14
	from temp_RowPerGroup_Keywords dsk
    group by VALUE_ID;
