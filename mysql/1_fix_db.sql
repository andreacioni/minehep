use hepdata;

-- AxisError: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T)
alter table AxisErrors drop key FK61BC228C1D3D1D62;

-- AxisProperties: rimossi indice ridondanti
alter table AxisProperties drop key FK70F3C7D42F51580B;
alter table AxisProperties drop key FK70F3C7D4B9695FBE;
alter table AxisProperties drop key PROPERTY_ID_index;

-- AxisReactions: rimossi indice ridondanti
alter table AxisReactions drop key FK2DD7D8A9B9695FBE;
alter table AxisReactions drop key REACTION_ID_index;
-- alter table AxisReactions drop key _yAxis_AXIS_ID_index; senza migliora di poco le performance, per ora lo tengo

/* BaseProperties: 
	- rimossi indice ridondanti, uniformato le unità di misura
	- Focus=HighValue quando HighValue=LowValue (HighValue e LowValue != null)
*/
alter table BaseProperties drop key PROPERTY_ID_index;
set SQL_SAFE_UPDATES = 0;
update BaseProperties set Focus=HighValue where (HighValue is not null) and (LowValue is not null) and (LowValue=HighValue);
set SQL_SAFE_UPDATES = 1;

-- BaseReactions: vuota
drop table BaseReactions;

/* Bins: 
	- Description probabilmente inutile
	- Focus è la media tra HighValue e LowValue quando Focus è vuoto
*/
alter table Bins drop column Description;
set SQL_SAFE_UPDATES = 0;
update Bins set Focus=((HighValue+LowValue)/2) where (HighValue is not null) and (LowValue is not null) and (Focus is null);
set SQL_SAFE_UPDATES = 1;

-- DatasetComments: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T)
alter table DatasetComments drop key FKC3AAB66CC83ABA29;

-- DatasetErrors: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T)
alter table DatasetErrors drop key FK57B405E3C83ABA29;

-- DatasetProperties: vuota
drop table DatasetProperties;

-- Datasets: rimossi indice ridondanti, colonna DataType eliminata perchè sempre nulla
alter table Datasets drop key DATASET_ID_index;
alter table Datasets drop key FK6E8B3F1BF4974CDB;
alter table Datasets drop column DataType;

-- DsKeywords: setto a null dove è vuoto
set SQL_SAFE_UPDATES = 0;
delete from DsKeywords where Keyword is null or trim(Keyword) = '';
set SQL_SAFE_UPDATES = 1;

/* DsObservables: 	- rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T)
					- 'pulisco' questa tabella dagli Observable nulli o vuoti (16 righe)
*/
alter table DsObservables drop key FK3A475F61C83ABA29;
set SQL_SAFE_UPDATES = 0;
delete from DsObservables where DsObservables is null or trim(DsObservables) = '';
set SQL_SAFE_UPDATES = 1;
-- DsPlabs: probabilmente inutile
drop table DsPlabs;

-- DsReactions: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T); setto a null dove è vuoto
alter table DsReactions drop key FK7E3FD77BC83ABA29;
set SQL_SAFE_UPDATES = 0;
delete from DsReactions where DsReactions is null or trim(DsReactions) = '';
set SQL_SAFE_UPDATES = 1;

-- FinalStates: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T), PDGCode sempre nullo; aggiungo una colonna con un flag F che mi tornerà utile nelle fasi successive
alter table FinalStates drop key REACTION_ID_index;
alter table FinalStates drop column PDGCode;
set SQL_SAFE_UPDATES = 0;
alter table FinalStates add column InitialFinalFlag char(1) not null default 'F';
set SQL_SAFE_UPDATES = 1;

-- InitialStates: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T); PDGCode sempre nullo; aggiungo una colonna con un flag I che mi tornerà utile nelle fasi successive
alter table InitialStates drop key REACTION_ID_index;
alter table InitialStates drop column PDGCode;
set SQL_SAFE_UPDATES = 0;
alter table InitialStates add column InitialFinalFlag char(1) not null default 'I';
set SQL_SAFE_UPDATES = 1;

-- Keywords: vuota 
drop table Keywords;

-- PaperAuthors: manca chiave primaria
-- alter table PaperAuthors add column ID bigint(20) primary key auto_increment; 

-- PaperComments: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T)
alter table PaperComments drop key FKDEB537808170C169;

-- PaperExpts: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T); colonna: Collider non usata
alter table PaperExpts drop key FK9CC777508170C169;
alter table PaperExpts drop column Collider;
alter table PaperExpts drop column ExptComment;
set SQL_SAFE_UPDATES = 0;
update PaperExpts set InformalName=null where InformalName is null or trim(InformalName) = '';
update PaperExpts set Lab=null where Lab is null or trim(Lab) = '';
update PaperExpts set ExptName='<UNKNOWN>' where ExptName is null or trim(ExptName) = '' or ExptName='?';
set SQL_SAFE_UPDATES = 1;

-- PaperMods: manca chiave primaria; probabilmente inutile
-- alter table PaperMods add column ID bigint(20) primary key auto_increment; 
drop table PaperMods;

-- PaperRefs: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T); metto a null le date che non sono state insertite opure quele che non hanno senso
alter table PaperRefs drop key FK893559CC8170C169;
set SQL_SAFE_UPDATES = 0;
update PaperRefs set Date=null where Date is null or trim(Date) = '';
update PaperRefs set Date='1964' where Date = '1064';
update PaperRefs set Date='1002' where Date = '1993';
alter table PaperRefs add Year smallint;

update PaperRefs set Year = case when Date then convert(Date, unsigned) else null end;
set SQL_SAFE_UPDATES = 1;

alter table PaperRefs drop Date;

/* Papers: 
	-rimozione indici duplicati; campi ignoti rimossi;
	-placeholder dove non è specificato il titolo;
	-dove il DOI è vuoto metto a null; 
	-dove il DOI è DUPLICATO svuoto e metto null
		select DOI, count(*) c 
		from Papers group by DOI having c > 1
		order by c desc;
        --->
		10.1007/BF01421774
		10.1007/BF01577559
		10.1007/BF02728263
		10.1016/0370-2693(81)90708-5
		10.1016/0370-2693(87)90139-0
		10.1016/0550-3213(80)90223-0
		10.1103/PhysRev.100.1802
		10.1103/PhysRevD.26.543
?c?

*/
alter table Papers drop key PAPER_ID_index;
alter table Papers drop column RedId;
alter table Papers drop column SpiresId;
alter table Papers drop column CdsId; 
--  rimuovo una Paper perchè non ha ne un titolo ne un InspireId
set SQL_SAFE_UPDATES = 0;
delete from Papers where InspireId is null;
update Papers set Title='<NO TITLE>' where Title is null or trim(Title) = '';
update Papers set DOI=null where trim(DOI) = '';

update Papers set DOI=null where DOI in ('10.1007/BF01421774', '10.1007/BF01577559', 
'10.1007/BF02728263', '10.1016/0370-2693(81)90708-5',
'10.1016/0370-2693(87)90139-0', '10.1016/0550-3213(80)90223-0',
'10.1103/PhysRev.100.1802', '10.1103/PhysRevD.26.543',
'?c?');

set SQL_SAFE_UPDATES = 1;

-- PointErrors: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T); PlusErrorLength tutti i valori nulli; Comment non utilizzato (campo testuale)
alter table PointErrors drop key FK5AAFECFBEAC63F68;
-- alter table PointErrors drop column PlusErrorLength; mi serve per la union nel create_star_schema

-- Points: ConfLevel sempre null
alter table Points drop column ConfLevel; 

-- Systematics: probabilmente inutile
drop table Systematics; 

-- XAxes: campo Unit sempre vuoto;
alter table XAxes drop column Unit;

/* YAxes: 	- rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T)
			- colonna Unit non usata 
            - metto a null tutti gli Observable vuoti (1605 righe)

*/
alter table YAxes drop key FK505871E3BAA6EF;
alter table YAxes drop key AXIS_ID_index;
alter table YAxes drop column Unit;
set SQL_SAFE_UPDATES = 0;
update YAxes set Observable = null where trim(Observable) = '';
set SQL_SAFE_UPDATES = 1;

-- YAxisComments: rimossi indice ridondanti (vedi: http://bit.ly/2uMaB7T)
alter table YAxisComments drop key FK57AB8FAE1D3D1D62;

